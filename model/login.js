var express = require('express');
var Promise = require('bluebird');
var model = require('./database.js');
var router = express.Router();

router.post('/checkLoginItems', function(req, res, next){
	if(!req.xhr){
		res.end();
	}else{
		var body = req.body;
		model.checkUsed(body).then(function(result){
			res.contentType('application/json');
			res.send(result);
		}).catch(console.log)
	}
});


router.post('/signup', function(req, res, next){
	var body = req.body;
	body["password"] = body["password"][0];
	body['create_date'] = new Date();
	body['modified_date'] = new Date();
	body["friends"] = [];
	body["group"] = [];
	body['request'] = [];
	body['birthDay'] = "";
	body['firstName'] = "";
	body['familyName'] = "";
	body['firstName_hurigana'] = "";
	body['familyName_hurigana'] = "";
	body['firstName_romaji'] = "";
	body['familyName_romaji'] = ""
	body['showName'] = body["user_name"];
	body['comment'] = "";
	body["accessTime"] = 1;
	model.signup(body).then(function(accessTokens){
		res.cookie("ACCTK", accessTokens['accessToken']);
		res.cookie("STID", accessTokens['STid']);
		res.type("json");
		res.send({"message":"true"});
	}).catch(function(message){
		res.type("json");
		res.send(message);
	})
});

router.post('/login', function(req, res, next){
	var body = req.body;
	var queryKey = {};
	var queryPass = {};
	if(body["loginKey"].indexOf("@") >= 0){
		queryKey["email"] = body["loginKey"];
	}else{
		queryKey["user_name"] = body["loginKey"];
	}
	queryPass["password"] = body["password"];

	model.login(queryKey, queryPass).then(function(accessTokens){
		res.cookie("ACCTK", accessTokens['accessToken']);
		res.cookie("STID", accessTokens['STid']);
		res.redirect("/");
	}).catch(function(errorMessage){
		req.session.loginfail = {message:"fail"}
		res.redirect('/login/loginfailed')
	});
});

var errorHandler = function(req, res){
	req.session.errors = {message:"Bad Request"};
	req.redirect("/error");
}

module.exports = router;
