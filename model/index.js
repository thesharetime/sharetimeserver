var express = require('express');
var Promise = require('bluebird');
var jws = require('../jws.js');
var model = require('./database.js');
var router = express.Router();
var fs = require('fs');

router.all("/*", function(req, res, next){
	if(req.xhr){
		next();
	}else{
		res.redirect("/");
	}
});

router.get('/getGroupInfo', function(req, res, next){
	var query = req.query;
	model.getGroupInfo(query["id"]).then(function(result){
		res.contentType("json");
		res.send(result);
	})
})

router.get('/requestMemberComm', function(req, res, next){
	var body = req.query;
	var id = req.cookies.STID;
	model.getCommons(id, body['id']).then(function(result){
		res.contentType("json");
		res.send(result);
	})
})

router.get('/requestMemberInfo', function(req, res,next){
	var body = req.query;
	console.log(body);
	var accessToken = req.cookies.ACCTK;
	model.requestUserInfo(body, accessToken).then(function(result){
		res.contentType("json");
		res.send(result);
	});
});

router.post("/editGroups", function(req, res, next){
	var body = req.body;
	var accessToken = req.cookies.ACCTK;
	model.editGroupsInfo(accessToken, body).then(function(newGroups){
		res.contentType("json");
		res.send(newGroups)
	});
});

router.post('/userIconUpload', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	var file = 	req.files;
	model.uploadIcon(accessToken, file).then(function(newGroupItems){
		res.contentType("json");
		res.send(newGroupItems);
	});
});

router.post('/promiseOffer', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var body = req.body;
	model.promiseOffer(accessToken, body).then(function(result){
		res.contentType("json");
		res.send(result);
	})
})

router.post('/addGroupEvent', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	var body = req.body;
	model.addGroupEvent(accessToken, body).then(function(result){
		res.contentType("json");
		res.send(result);
	});
});

router.post('/absenceGroupEvent', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	var body = req.body;
	model.absenceGroupEvent(accessToken, body).then(function(result){
		res.contentType("json");
		res.send(result);
	})
})

router.post('/attendGroupEvent', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	var body = req.body;
	model.attendGroupEvent(accessToken, body).then(function(result){
		res.contentType("json");
		res.send(result);
	})
})

router.post('/makeGroup', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	var body = req.body;
	var groupQuery = {};
	if(!body["inviteUser"]){
		body["inviteUser"] = [];
	}
	groupQuery["comment"] = body["comment"];
	groupQuery["groupName"] = body["groupName"];
	model.makeGroup(accessToken, groupQuery, body["inviteUser"], body["name"]).then(function(result){
		res.contentType("json");
		res.send(result);
	})
})

router.post('/inviteFriendsToGroup', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var body = req.body;
	model.inviteFriendsToGroup(accessToken, body).then(function(){
		res.contentType("json");
		res.send({"message":"done"});
	})
})




router.post('/addleisuretime', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	var item = req.body;
	item['addToFriends'] = JSON.parse(item['addToFriends']);
	model.addLeisure(accessToken, item).then(function(dayItem){
		res.contentType("json");
		res.send(dayItem);
	})
})


router.post('/requestfrienduser', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	var body = req.body;
	model.requestFriend(accessToken, body["id"], body['name']).then(function(result){

	})
})

router.post('/acceptRequestFriend', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	var body = req.body;

	model.acceptRequestFriend(accessToken, body["user_name"], body["id"], body["friend_name"]).then(function(events){
		res.contentType("json");
		res.send(events);
	})
});

router.post("/rejectrequestfriend", function(req, res, next){
	console.log("this");
	var accessToken = req.cookies.ACCTK;
	var body = req.body;
	model.rejectRequestFriend(accessToken, body.id).then(function(noticeList){
		res.contentType("json");
		res.send(noticeList);
	});
});

router.post('/removerequestfriend', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	var body = req.body;
	model.removeRequestFriend(accessToken, body["id"]).then(function(reques){
		res.contentType("json");
		res.send(request);
	});
});

router.post("/sendOfferMsg", function(req, res, next){
	var body = req.body;
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	model.sendOffer(accessToken, body).then(function(result){
		res.contentType("json");
		res.send(result);
	})
})

router.get("/getEventDetail", function(req, res, next){
	var body = req.query;
	model.getEventDetail(body).then(function(result){
		res.contentType("json");
		res.send(result);
	});
})

router.get("/getOfferMsgDay", function(req, res, next){
	var body = req.query;
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	model.getMsgToday(accessToken, body).then(function(result){
		res.contentType("json");
		res.send(result);
	})
})


router.post('/editmyprofile', function(req, res, next){
	var body = req.body;
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	var item = body["data"];
	model.editProfile(accessToken, item).then(function(result){
		res.contentType("json");
		res.send(result);
	})
})


router.get('/searchMember', function(req, res, next){
	var body = req.query;
	var query = [];
	if('q0' in body){
		for(var keys in body){
			query.push({"familyName":body[keys]});
			query.push({"firstName":body[keys]});
			query.push({"familyName_hurigana":body[keys]});
			query.push({"firstName_hurigana":body[keys]});
		}
	}else{
		query = [body];
	}
	model.searchMember(query).then(function(result){
		res.contentType('json');
		res.send(result);
	});
})

router.post('/confirmGroupInvite', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	var body = req.body;
	var query = {"id":body["id"], "groupName":body["groupName"]};
	var user_name = body["user_name"];
	model.confirmGroupInvite(accessToken, query, user_name).then(function(items){
		res.contentType('json');
		res.send(items);
	})
})

router.post('/removeShareTime', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var shareTimeID = req.body["timeID"];
	model.removeShareTime(accessToken, shareTimeID).then(function(result){
		var statusResult = {"state":true};
		res.contentType("json");
		res.send(statusResult);
	})
})

router.post("/editGroupEvent", function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var body = req.body;
	model.editGroupEvent(accessToken, body).then(function(result){
		res.contentType("json");
		res.send(result);
	});
});


router.post('/rejectGroupInvite', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	var body = req.body;
	model.rejectGroupInvite(accessToken, body).then
})

router.get('/getmainpageitems', function(req, res, next){
	var accessToken = req.cookies.ACCTK;
	var id = req.cookies.STID;
	model.getUserItems(accessToken).then(function(result){
		res.contentType("json");
		res.send(result);
	}).catch();
});

module.exports = router;
