var express = require("express");
var Promise = require("bluebird");
var jws = require("../jws.js");
var model = require("./database.js");
var router = express.Router();

router.get("/api/testGet", function(req, res){
	var queryPass = {password:"tsubasa"};
	var queryKey = {email:"tsubasa.shimomura@gmail.com"};
	model.login(queryKey, queryPass).then(function(accessTokens){
		model.getUserItems(accessTokens['accessToken']).then(function(result){
			res.contentType("json");
			res.send(result);
		});
	});
});

module.exports = router;