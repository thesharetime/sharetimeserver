var express = require("express");
var Promise = require("bluebird");
var model = require("./database.js");
var router = express.Router();



router.post("/passwordSend", function(req, res){
	model.sendEmail(req.body["email"]).then(function(result){
		console.log(result);
		res.contentType("json");
		res.send({message:"done"});
	});
});

router.post("/reflesh/:id", function(req, res){
	var id = req.params.id;
	var pass = req.body;
	if(pass["password"] != pass["passwordRe"]){
		req.session.errors = {messages:"Bad Request"};
		res.redirect("/error");
	}else{
		model.passwordReflesh(pass["password"], id).then(function(mes){
			res.contentType("json");
			res.send(mes);
		});
	}
})

module.exports = router;
