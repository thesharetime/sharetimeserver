var mongodb = require("mongodb");
var conf = require('config');
var mongoose = require('mongoose');
var jws = require('../jws.js');
var Promise = require('bluebird');
var fs = require('fs');
var bson = require('bson');
var romaji = require("romaji");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var makeObject = mongoose.Types.ObjectId;
var profileEditValidataKey = {"sendDataProfileComment":"comment", "sendDataProfileShowName":"showName", "sendDataProfileBirthDay":"birthDay", "sendDataProfileFirstName":"firstName", "sendDataProfileFamilyName":"familyName", "sendDataProfileFirstName_hurigana":"firstName_hurigana", "sendDataProfileFamilyName_hurigana":"familyName_hurigana"};

var sendgrid = require("sendgrid")(
		"app39455268@heroku.com",
		"snwrrzck0289"
);

var publicKeySchema = new Schema({
	priv:String,
	pub:String
});

var noticeFriendsSchema = new Schema({
	"_id":false,
	"name":{type:String, required:true},
	"id":{type:String, required:true}
});

var passwordRefleshSchema = new Schema({
	"userID":ObjectId,
	createdAt:{type:Date, expires: 60 * 15, default:Date.now}
});

var accessTokenRemakeSchema = new Schema({

})

var noticeInviteSchema = new Schema({
	"_id":false,
	"groupName":String,
	"groupId":ObjectId,
	"inviteFromName":String,
	"inviteFromId":ObjectId,
	"date":Date
});


var noticeGroupEventSchema = new Schema({
	"_id":{type:ObjectId, required:true, index:true},
	"groupName":String,
	"groupId":ObjectId,
	"note":String,
	"eventId":ObjectId,
	"date":Date,
	"type":String,
	expiresAt:{type:Date, required:true}
});

noticeGroupEventSchema.index({expiresAt:1}, {expireAfterSeconds:0});

var noticeSchema = new Schema({
	"_id":false,
	"name":String,
	"userId":ObjectId,
	"note":String,
	"optionId":ObjectId,
	"date":Date,
	"type":String
})

var friendsSchema = new Schema({
	"name":String,
	"id":ObjectId,
	"_id":false
});

var anyIdSchema = new Schema({
	"_id":false,
	"id":ObjectId
});

var anyIdUniqueSchema = new Schema({
	"_id":false,
	"id":{type:ObjectId}
});

var myGroupSchema = new Schema({
	"_id":false,
	"id":ObjectId
})

var userSchema = new Schema({
	"email":{type:String, unique:true, index:true, required:true},
	"user_name":{type:String, unique:true, index:true, required:true},
	"firstName":String,
	"familyName":String,
	"fullName":String,
	"fullName_hurigana":String,
	"fullName_romaji":String,
	"firstName_hurigana":String,
	"familyName_hurigana":String,
	"firstName_romaji":String,
	"familyName_romaji":String,
	"showName":String,
	"birthDay":String,
	"comment":String,
	"group":[{type:ObjectId, ref:conf.DB.COLLECTION.GROUP}],
	"request":[{type:String}],
	"password":{type:String, required:true},
	"friends":[{type:ObjectId, ref:conf.DB.COLLECTION.USERS}],
	"notice":[noticeSchema],
	"noticeFriends":[{type:ObjectId, ref:conf.DB.COLLECTION.USERS}],
	"noticeGroupInvite":[noticeInviteSchema],
	"noticeGroupEvent":[{type:ObjectId, ref:conf.DB.COLLECTION.NOTICE_GROUP_EVENT}],
	"modified_date":Date,
	"create_date":Date,
	"last_login":Date,
	"accessTime":Number,
	"accessAttempt":Number,
	"accessAttemptLast":{type:Date, default:Date.now},
	"accessLockOutToTime":{type:Date, default:Date.now}
});


var groupSchema = new Schema({
	"groupName":{type:String, required:true},
	"leader":{type:ObjectId, required:true, ref:conf.DB.COLLECTION.USERS},
	"alreadyInvite":[
		{type:String}
	],
	"member":[{type:ObjectId, ref:conf.DB.COLLECTION.USERS}],
	"notice":[noticeSchema],
	"comment":String,
	"modified_date":Date,
	"create_date":Date
})

var calendarIndividualSchema = new Schema({
	"owner_id":{type:ObjectId, unique:true, index:true, required:true, ref:Users},
	"days_id_my":[{type:ObjectId, ref:conf.DB.COLLECTION.DAYS}],
	"days_id_all":[{type:ObjectId, ref:conf.DB.COLLECTION.DAYS}],
	"days_id_member":[{type:ObjectId, ref:conf.DB.COLLECTION.DAYS}],
	"days_id_group":[{type:ObjectId, ref:conf.DB.COLLECTION.GROUP_EVENT}]
});

var calendarGroupSchema = new Schema({
	"group_id":{type:ObjectId, unique:true, index:true,required:true, ref:conf.DB.COLLECTION.GROUP},
	"days_id_group":[{type:ObjectId, ref:conf.DB.COLLECTION.GROUP_EVENT}],
	"event_id_group":[{type:ObjectId, ref:conf.DB.COLLECTION.GROUP_EVENT}]
});

var groupDaySchema = new Schema({
	"title":{type:String, require:true},
	"start":{type:String, require:true},
	"end":{type:String, require:true},
	"description":{
		"_id":false,
		"comment":String,
		"appearance":Boolean,
		"reserveById":String,
		"reserveByName":String,
		"eventTimeString":String,
		"dayString":String
	}
})

var eventSchema = new Schema({
	"title":{type:String, require:true},
	"start":{type:String, require:true},
	"end":{type:String, require:true},
	"description":{
		"_id":false,
		"suggestionBy":{type:ObjectId, ref:conf.DB.COLLECTION.USERS},
		"comment":String,
		"eventTimeString":String,
		"dayString":String,
		"attend":[{type:ObjectId, ref:conf.DB.COLLECTION.USERS}],
		"groupID":{type:ObjectId, ref:conf.DB.COLLECTION.GROUP},
		"groupName":String
	}
})

var daysSchema = new Schema({
	"title":{type:String, require:true},
	"start":{type:String, require:true},
	"end":{type:String, require:true},
	"description":{
		"_id":false,
		"owner_id":String,
		"owner_name":String,
		"eventTimeString":String,
		"dayString":String,
		"promiseWith":{
			"_id":false,
			"id":ObjectId,
			"name":String
		}
	}
});

var offerSchema = new Schema({
	"day_id":{type:ObjectId, index:true, required:true},
	"aboutDate":String,
	"aboutDay":String,
	"msg":String,
	"fromId":{type:ObjectId, required:true},
	"fromName":String,
	"toId":{type:ObjectId, required:true},
	"toName":String,
	"date":String,
});

mongoose.model(conf.DB.COLLECTION.PASSWORD_REFLESH, passwordRefleshSchema);
mongoose.model(conf.DB.COLLECTION.OFFER_MESSAGE, offerSchema);
mongoose.model(conf.DB.COLLECTION.GROUP, groupSchema);
mongoose.model(conf.DB.COLLECTION.USERS, userSchema);
mongoose.model(conf.DB.COLLECTION.CALENDAR_INDIVIDUAL, calendarIndividualSchema);
mongoose.model(conf.DB.COLLECTION.CALENDAR_GROUP, calendarGroupSchema);
mongoose.model(conf.DB.COLLECTION.DAYS, daysSchema);
mongoose.model(conf.DB.COLLECTION.GROUP_EVENT, eventSchema);
mongoose.model(conf.DB.COLLECTION.GROUP_DAY, groupDaySchema);
mongoose.model(conf.DB.COLLECTION.NOTICE_GROUP_EVENT, noticeGroupEventSchema);
mongoose.model("PublicAndPrivatePems", publicKeySchema);


var db = mongoose.createConnection('mongodb://' + conf.DB.mongodbHerokuURI, function(){

});

exports.mongooseConnection = db;

var Users = db.model(conf.DB.COLLECTION.USERS);
var Groups = db.model(conf.DB.COLLECTION.GROUP);
var CalendarIndividual = db.model(conf.DB.COLLECTION.CALENDAR_INDIVIDUAL);
var CalendarGroup = db.model(conf.DB.COLLECTION.CALENDAR_GROUP);
var Days = db.model(conf.DB.COLLECTION.DAYS);
var Offer = db.model(conf.DB.COLLECTION.OFFER_MESSAGE);
var GroupDay = db.model(conf.DB.COLLECTION.GROUP_DAY);
var GroupEvent = db.model(conf.DB.COLLECTION.GROUP_EVENT);
var PasswordReflesh = db.model(conf.DB.COLLECTION.PASSWORD_REFLESH);
var NoticeGroupEvent = db.model(conf.DB.COLLECTION.NOTICE_GROUP_EVENT);
var PublicAndPrivatePems = db.model("PublicAndPrivatePems");



userSchema.path("friends").validate(function(value){
	console.log(this);
	return true;
}, "the user is already friends");

userSchema.path("request").validate(function(value){
	console.log(this);
	return true;
}, "you have send request to this user");


exports.keyGenOrKeyGet = function(){
	return new Promise(function(resolve, reject){
		PublicAndPrivatePems.findOne({}, function(err, result){
			if(err || !result){
				console.log(err);
				resolve()
			}else{
				reject(result);
			}
		});
	});
}

exports.savePublicKey = function(privGem,pubGem){
	var keys = {priv:privGem, pub:pubGem};
	console.log(keys);
	PublicAndPrivatePems.create(keys, function(err, keys){
	});
}

exports.checkUsed = function(item){
	return new Promise(function(resolve, reject){
		Users.find(item, function(err, result){
			if(err){
				reject(err);
			}else{
				var state = (result.length != 0) ? {"state":false} : {"state":true};
				resolve(state);
			}
		});
	});
};

exports.login = function(loginKey, loginPass){
	return new Promise(function(resolve, reject){
		var now = new Date();
		Users.findOne(loginKey, function(err, result){
			if(err){
				reject(err);
			}else if(result){
				if(result["accessLockOutToTime"].getTime() > now.getTime()){
					reject({"state":false, "message":"this account is lock out because accessing many time while 10 minute"});
				}else{
					jws.comparePass(loginPass["password"], result["password"]).then(function(flag){
						if(!flag){
							if(now.getTime() - result["accessAttemptLast"].getTime() < 1000 * 60 * 10){
								if(result["accessAttempt"] > 9){
									Users.update({"_id":result["_id"]}, {"$set":{"accessLockOutToTime":now.setMinutes(now.getMinutes() + 10)}}, function(err, result){
										reject();
									});
								}else{
									Users.update({"_id":result["_id"]}, {"$set":{"accessAttemptLast":now}, "$inc":{"accessAttempt":1}}, function(err, result){
										reject(err);
									});
								}
							}else{
								Users.update({"_id":result["_id"]},  {"$set":{"accessAttemptLast":now, "accessAttempt":1}}, function(err, result){
									reject(err);
								});
							}
						}else{
							jws.makeAccessToken(result["_id"]).then(function(accessToken){
								resolve(accessToken);
							});
						}
					});
				}
			}else{
				reject(err);
			}
		})
	})
}

exports.checkSumRefreshPassword = function(refleshToken){
	return new Promise(function(resolve, reject){
		PasswordReflesh.findById(new makeObject(refleshToken), function(err, result){
			if(result){
				resolve(result);
			}else{
				reject();
			}
		});
	});
}

exports.editGroupsInfo = function(accessToken, query){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			Groups.findOne({"_id":new makeObject(query.groupID),"leader":new makeObject(id)}, function(err, result){
				if(err){
					console.log(err);
					reject();
				}else{
					Groups.findByIdAndUpdate({"_id":result._id}, {"$set":{"groupName":query.newGroupName, "comment":query.newGroupComment}}, {"select":"leader"}).populate("leader", "showName _id").exec(function(err, groups){
						if(err){
							reject();
						}else{
							var items = {"groupName":query.newGroupName, "comment":query.newGroupComment, "leader":{"showName":groups.leader.showName, "_id":groups.leader._id}};
							resolve(items);
						}
					});
				}
			});
		}
	});
}

exports.signup = function(signupItem){
	return new Promise(function(resolve, reject){
		if(signupItem["email"].length == 0){
			reject({"message":false, "type":"email"});
		}else if(signupItem["password"].length == 0){
			reject({"message":false, "type":"password"});
		}else if(signupItem["user_name"].length == 0){
			reject({"message":false, "type":"userName"});
		}else{
			jws.cipherPass(signupItem['password']).then(function(passCipherItem){
				signupItem['password'] = passCipherItem["pass"];
				Users.create(signupItem, function(err, user){
					if(err){
						console.log(err);
						reject({"message":false, "type":"internalError"});
					}else{
						CalendarIndividual.create({
							"owner_id":user._id,
							"days_id_my":[],
							"days_id_all":[],
							"days_id_member":[],
							"days_id_group":[]
						});
						resolve(jws.makeAccessToken(user._id));
					}
				});
			});
		}
	});
}


exports.getUserItems = function(accessToken){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			Promise.all([getUserInfo(id), getsUserCalendarItem(id)]).then(function(result){
				var groupId = result[0]["group"];
				var user_info = result[0];
				var calendar = result[1];
				var friendsCal = calendar["memberEvents"];
				var friends = user_info["friends"];
				for(var i = 0, len = friendsCal.length ; len > i; i++){
					for(var j = 0, len1 = friends.length; len1 > j; j++){
						if(friendsCal[i].description.owner_id == friends[j].id){
							calendar["memberEvents"][i]["description"]["owner_name"] = friends[j].name;
							break;
						}
					}
				}

				if(groupId.length > 0){
					var query = []
					for(var i = 0; groupId.length > i; i++){
						query.push({"_id":new makeObject(groupId[i]["id"])});
					}
					Groups.find({"$or":query}, 'groupName leader').populate("leader", "showName").exec(function(err, groups){
						var groupCal = calendar["groupEvents"];

						for(var i = 0, len = groupCal.length; len > i; i++){
							for(var j = 0, len1 = groups.length; len1 > j; j++){
								if(groupCal[i].description.groupID == groups["id"]){
									calendar["groupEvents"][i]["groupName"] = groups[j]["name"];
									break;
								}
							}
						}						
						resolve({"user_info":user_info, "calendar":calendar, "group":groups});
					})
				}else{
					resolve({"user_info":user_info, "calendar":calendar, "group":groupId});
				}

			});
		}else{
			reject("error");
		}
	});
}

exports.getCommons = function(id,friendId){
	return new Promise(function(resolve, reject){
		var query = [{"_id":new makeObject(id)},{"_id":new makeObject(friendId)}];
		Users.find({"$or":query}).populate("friends").populate("group").exec(function(err,result){
			var user1 = result[0];
			var user2 = result[1];
			console.log(result);
			Promise.all([commonUser(user1['friends'], user2['friends']), commonGroup(user1['group'], user2['group'])]).then(function(result){
				var commons = {"commonUser":result[0], "commonGroup":result[1]};
				resolve(commons);
			})
		})
	})
}


exports.requestUserInfo = function(body, accessToken){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			console.log(body);
			var query = [{"_id":new makeObject(id)},{"_id":new makeObject(body.id)}];
			Users.find({"$or":query}, "firstName familyName user_name showName birthDay comment group friends").populate("friends").populate("group").exec(function(err, result){
				if(err){
					console.log(err);
				}else{
					var client;
					var friend;
					if(result[0]._id.toString() != id){
						friend = result[0];
						client = result[1];
					}else{
						friend = result[1];
						client = result[0];
					}
					Promise.all([commonUser(client.friends, friend.friends), commonGroup(client.group, friend.group)]).then(function(result){
						console.log(result);
						var commons = {"commonUser":result[0], "commonGroup":result[1]}
						resolve({friendItem:friend, common:commons});
					});
				}
			});
		}
	});
}

exports.getGroupInfo = function(id){
	return new Promise(function(resolve, reject){
		Groups.findOne({"_id":new makeObject(id)}, "groupName leader comment member -_id").populate("leader", "showName").populate("member", "showName _id").exec(function(err, groups){
			resolve(groups);
		});
	});
}

exports.passwordReflesh = function(pass, id){
	return new Promise(function(resolve, reject){
		PasswordReflesh.findOneAndRemove({"_id":new makeObject(id)}, function(err, token){
			if(err){
				reject({"message":"anyError", state:false});
			}else{
				jws.cipherPass(pass).then(function(newPass){
					Users.uppdate({_id:token["userID"]}, {"password":newPass["pass"]}, function(err, result){
						resolve({"message":"success", state:true});
					});
				});
			}
		})
	})
}

exports.sendEmail = function(emailAddress){
	return new Promise(function(resolve, reject){
		Users.findOne({"email":emailAddress}, function(err, result){
			if(result){
				PasswordReflesh.create({userID:result._id},
					function(err, passToken){
						sendgrid.send({
							to:emailAddress,
							from: "app39455268@heroku.com",
							subject:"パスワード再設定",
							text:"https://sharetimeproject.herokuapp.com/password/reflesh/" + passToken._id.toString()
						}, function(err, json){
							resolve("done");
						})
					});
			}else{
				console.log("no exist");
				resolve("done");
			}
		});
	})
}

exports.addLeisure = function(accessToken, items){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			var memberIds = []

			if(items["type"] == 1){
				Users.findById({"_id":new makeObject(id)}, "friends", function(err, result){
					for(var i = 0; result["friends"].length > i; i++){
						memberIds.push(result["friends"][i]);
					}

				});
			}else{
				var members = items['addToFriends'];
				for(var i = 0; members.length > i ; i++){
					memberIds.push(members[i]["id"]);
				}
			}

			addDayUsersCalendar(items, memberIds, id).then(function(day){
				resolve(day);
			});
		}
	});
}

var idIndexOf = function(arr ,id){
	for(var i = 0; arr.length > i; i++){
		if(arr[i]["id"] == id){
			return i;
		}
	}
	return -1;
}

var idLastIndexOf = function(arr, id){
	for(var i = arr.length - 1; i >= 0; i--){
		if(arr[i]["id"] == id){
			return i;
		}
	}
	return -1;
}

exports.uploadIcon = function(accessToken, file){
	return new Promise(function(resolve, reject){
		var id;
		if(jws.verifyAccess(accessToken)){
			var target_path, tmp_path;
			tmp_path = file.thumbnail.path;
			target_path = "./user_icon" + file.thumbnail.name;
			fs.rename(tmp_path, target_path, function(err){
				if(err){
					reject(err);
				}
				fs.unlink(tmp_path, function(err){
					if(err){
						reject(err);
					}
					resolve();
				})
			});
		}
	})
}

exports.searchMember = function(query){
	return new Promise(function(resolve, reject){
		Users.find({"$or":query},"showName",function(err,result){
			resolve(result);
		})
	})
}

exports.editProfile = function(accessToken, body){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			var query = {};
			for(key in body){
				if(profileEditValidataKey[key]){
						query[profileEditValidataKey[key]] = body[key];
				}else{
						reject();
						return;
				}
			}

			query["familyName_romaji"] = query.familyName_hurigana ? romaji.fromKana(query.familyName_hurigana):query.familyName;
			query["firstName_romaji"] = query.firstName_hurigana ? romaji.fromKana(query.firstName_hurigana):query.firstName;
			query["fullName"] = query.familyName + " " + query.firstName
			query["fullName_hurigana"] = query.familyName_hurigana ? query.familyName_hurigana + " " + query.firstName_hurigana : "";
			query["fullName_romaji"] = query.familyName_romaji + " " + query.firstName_romaji;
			query["modified_date"] = new Date();

			Users.findByIdAndUpdate({"_id":new makeObject(id)},{"$set":query},{"select":"birthDay familyName comment firstName familyName familyName_hurigana firstName_hurigana fullName fullName_hurigana showName user_name"},function(err, result){
				resolve(query);
			});
		}
	})
}

/*
exports.getCommons = function(id,friendId){
	return new Promise(function(resolve, reject){
		var query = [{"_id":new makeObject(id)},{"_id":new makeObject(friendId)}];
		Users.find({"$or":query}).populate("friends").populate("group").exec(function(err,result){
			var user1 = result[0];
			var user2 = result[1];
			console.log(result);
			Promise.all([commonUser(user1['friends'], user2['friends']), commonGroup(user1['group'], user2['group'])]).then(function(result){
				var commons = {"commonUser":result[0], "commonGroup":result[1]};
				resolve(commons);
			})
		})
	})
}
*/
exports.requestFriend = function(accessToken, friendId, name){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			addRequestFriend(id, friendId, name).then(function(){
				resolve();
			})
		}
	})
}

exports.removeRequestFriend = function(accessToken, friendId){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			removeRequestAddFriend(id, friendId).then(function(request){
				resolve(request);
			})
		}
	})
}

exports.rejectRequestFriend = function(accessToken, requestUserId){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			console.log(id);
			rejectFriendRequest(id, requestUserId).then(resolve).catch(function(err){
				reject();
			});
		}else{
			reject();
		}
	});
}

var rejectFriendRequest = function(receiveUser, requestUser){
	return new Promise(function(resolve, reject){
		var receiveUserID = new makeObject(receiveUser);
		var requestUserID = new makeObject(requestUser);
		Users.findOneAndUpdate({_id:receiveUserID}, {$pull:{noticeFriends:requestUserID}}, {select:"noticeFriends"}).populate("noticeFriends", "showName _id").exec(function(err, userNotice){
			resolve(userNotice.noticeFriends);
		});
		Users.update({_id:requestUserID}, {$pull:{request:receiveUser}}, function(err, result){
		});
	});
}

exports.getEventDetail = function(query){
	return new Promise(function(resolve, reject){
		GroupEvent.findById(new makeObject(query.id)).populate("description.attend", "showName").populate("description.groupID", "leader").populate("description.suggestionBy", "showName").exec(function(err, events){
			resolve(events);
		});
	});
}

exports.confirmGroupInvite = function(accessToken, body, user_name){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			var groupID = body.id;
			body["id"] = new makeObject(body["id"]);

			Groups.findOneAndUpdate({"_id":body.id, alreadyInvite:{$in:[id]}, member:{$nin:[new makeObject(id)]}}, {"$push":{"member":new makeObject(id)}, "$pull":{alreadyInvite:id}},{"select":"groupName leader"}).populate("leader", "showName").exec(function(err, joinGroup){
				console.log(joinGroup);
				console.log(err);
				if(joinGroup){
					CalendarGroup.findOne({"group_id":new makeObject(body["id"])}).populate("event_id_group").exec(function(err, calendar){
						resolve({"groupDays":calendar.event_id_group, "group":joinGroup});
						var eventid = [];
						for(var i = 0, len = calendar.event_id_group.length; len > i; i++){
							eventid.push(calendar.event_id_group[i]._id);
						}
						CalendarIndividual.findOneAndUpdate({"owner_id":new makeObject(id)}, {"$pushAll":{"days_id_group":eventid}}, function(err, result){
						});
					});
				}else{
					resolve({state:"already"});
				}
			});

			Users.findOneAndUpdate({"_id":new makeObject(id), group:{$nin:[body.id]}}, {"$push":{"group":body.id}, "$pull":{"noticeGroupInvite":{"groupId":new makeObject(body.id)}}}, function(err, result){
				console.log("users up date");
				console.log(result);
				console.log(err);
			});
		}
	})
}

exports.rejectGroupInvite = function(accessToken, body){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			var usersID = new makeObject(id);
			var groupID = new makeObject(body.id);
			Users.update({"_id":usersID}, {"$pull":{"noticeGroupInvite":{"groupId":groupID}}}, function(err, result){
			});
			Groups.update({"_id":groupID}, {"$pull":{"alreadyInvite":id}}, function(err, result){
			});
		}
	})
}

exports.makeGroup = function(accessToken, body, inviteUser, name){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			var leaders = new makeObject(id);
			body["leader"] = leaders
			body["member"] = [leaders]
			body["notice"] = [];
			body["create_date"] = new Date();
			body["modified_date"] = new Date();
			body["alreadyInvite"] = inviteUser;
			Groups.create(body, function(err, group){
				Groups.populate(group, {path:"leader", select:"showName"}, function(err, group){
					if(inviteUser.length > 0){
						noticeInviteGroup(inviteUser, group["groupName"],group["_id"],name, id);
					}

					var groupReturn = {"_id":group["_id"], "groupName":group["groupName"], "leader":group["leader"]};
					resolve(groupReturn);
					Users.update({"_id":leaders}, {"$push":{"group":group._id}}, function(err, result){
					});
					CalendarGroup.create({"group_id":group._id, "days_id_group":[], "event_id_group":[]}, function(err, result){

					});

				})
			});
		}else{
			reject();
		}
	});
}


exports.acceptRequestFriend = function(accessToken, userName, friendId, friendName){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			acceptRequestFriendRequest(id, userName, friendId, friendName);
			connectEventCalendar(id, friendId);
			getsUserCalendarItem(friendId).then(function(calendar){
				Users.findById(new makeObject(friendId), "showName", function(err, friendItem){
					resolve({calendar:calendar["myEvents"], newFriend:friendItem});
				});
			});
		}
	})
}

exports.removeLeisure = function(accessToken, day_id){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			removeLeisure(day_id);
		}
	})
}


exports.editGroupEvent = function(accessToken, body){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			var newItems = body["newEventItem"];
			var groupId = body["groupID"];
			var editEventId = body["editEventID"];
			if(newItems){
				GroupEvent.findOneAndUpdate({"_id":new makeObject(editEventId), "description.suggestionBy":new makeObject(id)}, {"$set":{"start":newItems.start, "end":newItems.end, "title":newItems.title, "description.comment":newItems.comment, "description.eventTimeString":newItems.eventTimeString, "description.dayString":newItems.dayString}}).populate("description.groupID").exec(function(err, editedEvent){
					GroupEvent.findOne({"_id":editedEvent._id}).populate("description.groupID").exec(function(err, editedEvent){
						resolve({"edited":editedEvent, "type":true});
					});

					console.log(editedEvent);

					var member = editedEvent.description.groupID.member;
					var query = [];

					for(var i = 0, len = member.length; len > i; i++){
						if(member[i] != id){
							query.push({_id:new makeObject(member[i]), noticeGroupEvent:{$nin:[editedEvent._id]}});
						}
					}

					console.log(query);

					var dayString = newItems.dayString.split("/");
					var expiresAt = new Date(dayString[0]+"-"+dayString[1]+"-"+dayString[2]);

					var newNotice = {"groupName":editedEvent.description.groupID.groupName,	"groupId":editedEvent.description.groupID._id,"note":editedEvent.title,"eventId":editedEvent._id,"date":new Date(),"type":"edit","expiresAt":expiresAt}

					NoticeGroupEvent.findByIdAndUpdate(editedEvent._id, newNotice, function(err, result){
						Users.update({$or:query}, {$push:{noticeGroupEvent:editedEvent._id}}, {multi:true},function(err,result){
							console.log(err);
							console.log(result);
						});
					});

				});
			}else{
				GroupEvent.findByIdAndRemove({"_id": new makeObject(editEventId)}, function(err, removeEvent){
					resolve({"type":false});
					NoticeGroupEvent.remove({"_id":removeEvent._id}, function(err,result){
						console.log(err);
						console.log(result);
					});
				});
			}
		}
	});
}


exports.addGroupEvent = function(accessToken, body){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			makeGroupEvent(id, body).then(resolve);
		}
	})
}

var makeGroupEvent = function(id, body, noticeMsg){
	return new Promise(function(resolve, reject){
		body["description"]["attend"] = [new makeObject(id)];
		body["description"]["suggestionBy"] = new makeObject(id);
		body.description.groupID = new makeObject(body.description.groupID);
		console.log(body);
		GroupEvent.create(body,function(err, event){
			console.log(err);
			console.log(event);
			var groupID = event["description"]["groupID"]
				CalendarGroup.findOneAndUpdate({"group_id":groupID}, {"$push":{"event_id_group":new makeObject(event["_id"])}}).exec(function(err, result){
					if(err){
						console.log(err);
						reject();
					}else{

						Groups.findById(groupID).populate("member").exec(function(err, group){
							var member = group["member"];
							var members = []
							var memberIDs = []

							console.log(group);

							for(var i = 0; member.length > i ; i++){
								if(id != member[i]["id"].toString()){
									members.push({"_id":member[i]["_id"]})
								}
								memberIDs.push({"owner_id":new makeObject(member[i]["_id"])});
							}

							var dayString = body.description.dayString.split("/");
							var expiresAt = new Date(dayString[0]+"-"+dayString[1]+"-"+dayString[2]);
							console.log(expiresAt);

							var newNotice = {
								"_id":event._id,
								"groupName":group["groupName"],
								"groupId":group["_id"],
								"note":event["title"],
								"eventId":event["_id"],
								"expiresAt":expiresAt
							}
							
							if(noticeMsg){
								newNotice["type"] = noticeMsg;
							}else{
								newNotice["type"] = "new";
							}

							CalendarIndividual.update({"$or":memberIDs}, {"$push":{"days_id_group":event["_id"]}}, {multi:true},function(err, result){
								console.log(err);
								console.log(result);
							});

							noticeAddGroupEvent(members, newNotice);

						});
					}

					resolve(event);
				})
		});

	});
}

var noticeAddGroupEvent = function(members, newNotice){
	var notice = new NoticeGroupEvent(newNotice);
	notice.save(function(err){
		if(err){
			console.log(err);
		}

		Users.update({"$or":members}, {"$push":{"noticeGroupEvent":notice._id}}, {multi:true},function(err, result){
			console.log(err);
			console.log(result);
		});
	});
}

exports.attendGroupEvent = function(accessToken, body){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			var eventId = new makeObject(body["eventID"]);
			var groupId = new makeObject(body["groupID"]);
			var name = body["name"];
			if(body.noticeId != "none"){
				var noticeId = new makeObject(body.noticeId);
				Users.update({"_id":new makeObject(id)}, {"$pull":{"noticeGroupEvent":noticeId}}, function(err, result){
					if(err){
						reject()
					}else{

					}
				});
			}
			GroupEvent.update({"_id":eventId, "description.attend":{$nin:[new makeObject(id)]}}, {"$push":{"description.attend":new makeObject(id)}}, function(err, result){
				if(err){
					console.log(err);
				}else{
					console.log(result);
				}
			});

			resolve({state:"end"});
		}
	})
}

exports.removeShareTime = function(accessToken, shareTimeID){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			Days.findByIdAndRemove({"_id":new makeObject(shareTimeID), "description.owner_id":id}, function(err, result){
				resolve(result);
			});
		}
	});
}

/*
exports.getEventInfo = function(accessToken, body){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			var groupId = new makeObject(body["groupID"]);
			var eventId = new makeObject(body["eventID"]);

			function getGroupEventInfo(){
				return new Promise(function(resolve, reject){
					GroupEvent.findOne({"_id":eventId}, function(err, event){
						if(err){
							console.log(err)
						}else{
							resolve(event);
						}
					})
				})
			}

			function getGroupInfo(){
				return new Promise(function(resolve, reject){
					Groups.findOne({"_id":groupId}, function(err, group){
						if(err){
							console.log(err)
						}else{
							resolve(group);
						}
					})
				})
			}


			Promise.all([getGroupInfo(), getGroupEventInfo()]).then(function(results){
				var group = results[0];
				var events = results[1];

				resolve({"group":group, "event":events});
			})

		}
	})
}
*/

exports.absenceGroupEvent = function(accessToken, body){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			var eventId = new makeObject(body["eventID"]);
			if(body.noticeId != "none"){
				var noticeId = new makeObject(body.noticeId);
				Users.update({"_id":new makeObject(id)}, {"$pull":{"noticeGroupEvent":noticeId}}, function(err, result){
					if(err){
						console.log(err);
					}else{
						console.log(result);
					}
				});
			}
			GroupEvent.update({"_id":eventId, "description.attend":{$in:[new makeObject(id)]}}, {"$pull":{"description.attend":new makeObject(id)}}, function(err, result){
				console.log(err);
				console.log(result);
			});

			resolve({"state":"end"});
		}
	});
}

exports.promiseOffer = function(accessToken, body){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			var dayID = new makeObject(body["dayID"]);
			var offerFrom = new makeObject(body["fromID"]);
			var offerFromName = body["fromName"];
			var myName = body["myName"]
			var myID = new makeObject(id);
			Days.findByIdAndRemove({"_id":dayID}, function(err, day){
				if(day){
					var descriptions = day["description"];
					var newDayQuery = {
						"title":day["title"],
						"start":day["start"],
						"end":day["end"],
						"description":{
							"dayString":descriptions["dayString"],
							"eventTimeString":descriptions["eventTimeString"],
							"owner_id":descriptions["owner_id"],
							"owner_name":descriptions["owner_name"],
							"promiseWith":{
								"id":offerFrom,
								"name":offerFromName
							}
						}
					}

					Days.create(newDayQuery, function(err, newDay){
						resolve(newDay);
						CalendarIndividual.update({"$or":[{"owner_id":myID}, {"owner_id":offerFrom}]}, {"$push":{"days_id_my":newDay["_id"]}}, {multi:true},function(err, result){
						});
						promiseConfirm(offerFrom, myName, myID, newDay["_id"], newDay["description"]["eventTimeString"]);
					});
				}else{
					reject();
				}
			});
		}
	});
}

var array_in_id = function(arr, id){
	for(var i = 0 ; arr.length > i; i++){
		if(arr[i]["id"] == id){
			return true;
		}
	}

	return false;
}

var promiseConfirm = function(id, toName, toID, dayID, timeString){
	var notice = {
		name:toName,
		userId:toID,
		optionId:dayID,
		note:timeString,
		date:new Date(),
		type:"confirmPromise"
	}

	Users.update({"_id":id}, {"$push":{"notice":notice}}, function(err, result){
		console.log(result);
	})
}

exports.inviteFriendsToGroup = function(accessToken, body){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			Groups.findByIdAndUpdate({"_id":new makeObject(body["groupID"])},{$pushAll:{alreadyInvite:body.inviteUser}},function(err, group){
				console.log(group)
				if(body.inviteUser.length > 0){
					noticeInviteGroup(body["inviteUser"], group.groupName, group._id, body.name, id);
					resolve();
				}else{
					resolve();
				}
			})
			resolve();
		}else{
			reject();
		}
	});
}

exports.sendOffer = function(accessToken, body){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			Offer.create({
				"day_id":new makeObject(body["dayId"]),
				"msg":body["msg"],
				"fromId":new makeObject(id),
				"fromName":body["fromName"],
				"toId":new makeObject(body["toId"]),
				"toName":body["toName"],
				"aboutDate":body["aboutDate"],
				"aboutDay":body["aboutDay"],
				"date":body["date"]}, function(err, result){
					noticeOffer(result);
					resolve(result);
				}
			)
		}
	})
}

exports.getMsgToday = function(accessToken, body){
	return new Promise(function(resolve, reject){
		var id;
		if(id = jws.verifyAccess(accessToken)){
			var query;
			if(body["allDay"]){
				query = {"toId":new makeObject(id)};
			}else{
				query = {"toId":new makeObject(id), "aboutDay":body["day"]};
			}
			Offer.find(query, function(err, result){
				console.log(err);
				resolve(result);
			})
		}
	})
}


var removeLeisureFromDays = function(day_id){
	Days.remove({"_id":new makeObject(day_id)}, function(err, result){
	})
	Offer.remove({"day_id":new makeObject(day_id)}, function(err, result){
	})
}

var acceptRequestFriendRequest = function(id, userName, friendId, friendName){
	var friend = {"id":new makeObject(friendId), "name":friendName};
	var user = {"id":new makeObject(id), "name":userName};
	Users.findOneAndUpdate({"_id":user["id"], friends:{$nin:[friend.id]},noticeFriends:{$in:[friend.id]}}, {"$pull":{noticeFriends:friend.id}, "$push":{"friends":friend.id}}, function(err, accept){
		Users.findOneAndUpdate({"_id":friend["id"], friends:{$nin:[user.id]},request:{$in:[id]}}, {"$pull":{request:id}, "$push":{"friends":user.id, notice:{name:accept.showName, userId:accept._id, type:"acceptRequest"}}},function(err, result){
		});
	});
}

var connectEventCalendar = function(user_id, friend_id){
	CalendarIndividual.findOne({"owner_id":new makeObject(user_id)}, function(err, result){
		CalendarIndividual.update({"owner_id":new makeObject(friend_id)}, {"$pushAll":{"days_id_member":result["days_id_all"]}}, function(err, result){
			if(err){
			}else{
			}
		});
	});
	CalendarIndividual.findOne({"owner_id":new makeObject(friend_id)},function(err, result){
		CalendarIndividual.update({"owner_id":new makeObject(user_id)}, {"$pushAll":{"days_id_member":result["days_id_all"]}}, function(err, result){
			if(err){
			}else{
			}
		});
	});
}

var removeRequestAddFriend = function(id, friendId){
	return new Promise(function(resolve, reject){
		Users.findOneAndUpdate({"_id":new makeObject(id)}, {"$pull":{"request":friendId}},function(err, user){
			console.log(user);
			console.log(err);
			resolve(user.request);
		});
		Users.update({"_id":new makeObject(friendId)}, {"$pull":{"noticeFriends":new makeObject(id)}}, function(err, user){
			console.log(err);
			console.log(user);
		});
	});
}

var addRequestFriend = function(id, friendId, name){
	return new Promise(function(resolve, reject){
		var query = {"_id":new makeObject(id)}
		var friendsId = new makeObject(friendId);
		Users.update({"_id": new makeObject(id), request:{$nin:[friendId]}, friends:{$nin:[friendId]}}, {"$push":{"request":friendId}}, function(err, result){
			console.log(err);
			console.log(result);
			if(err || result.nModified === 0){
				console.log(err);
				resolve();
			}else{
				Users.update({"_id":friendsId, friends:{$nin:[new makeObject(id)]}, noticeFriends:{$nin:[new makeObject(id)]}}, {"$push":{"noticeFriends":query["_id"]}}, function(err, result){
				});
			}
		});
		resolve()
	})
}

var commonUser = function(user_friend1, user_friend2){
	return new Promise(function(resolve, reject){
		var tmp = user_friend1.concat(user_friend2);

		var friendArray = []

		for(var i = 0; tmp.length > i; i++){
			friendArray.push(tmp[i]["_id"].toString());
		}

		console.log(friendArray);

		var friendComm = [];

		for(var i = 0; friendArray.length > i ;i++){
			if(friendArray.indexOf(friendArray[i]) != friendArray.lastIndexOf(friendArray[i]) && friendComm.indexOf(friendArray[i]) < 0){
					friendComm.push({"_id":new makeObject(friendArray[i])});
			}
		}

		console.log(friendComm);

		if(friendComm.length > 0){
			Users.find({"$or":friendComm}, 'showName', function(err, result){
				console.log(err);
				console.log(result);
				resolve(result);
			});
		}else{
			resolve([]);
		}
	});
}

var commonGroup = function(user_group1, user_group2){
	return new Promise(function(resolve, reject){
		var tmp = user_group1.concat(user_group2);

		console.log(tmp)

		var groupsArray = [];
		for(var i = 0; tmp.length > i; i++){
			groupsArray.push(tmp[i]["_id"].toString());
		}

		console.log(groupsArray);

		var groupComm = [];

		for(var i = 0; groupsArray.length > i ;i++){
			if(groupsArray.indexOf(groupsArray[i]) !=　groupsArray.lastIndexOf(groupsArray[i])){
					groupComm.push({"_id":new makeObject(groupsArray[i])});
			}
		}

		if(groupComm.length > 0){
			Groups.find({"$or":groupComm}, 'groupName', function(err, result){
				resolve(result);
			});
		}else{
			resolve([]);
		}
	})
}

var getUserInfo = function(id){
	return new Promise(function(resolve, reject){
		var query = {"_id":new makeObject(id)};
		Users.findOneAndUpdate(query,{"$set":{"notice":[]}, "$inc":{"accessTime":1}},{"select":'user_name friends firstName familyName firstName_hurigana familyName_hurigana showName comment birthDay group request notice noticeFriends noticeGroupInvite noticeGroupEvent accessTime'}).populate("noticeGroupEvent").populate("noticeFriends", "showName _id").populate("friends", "showName").exec(function(err, result){
			if(err){
				reject(err);
			}else{
				console.log(err);
				resolve(result);
			}
		});
	});
}

var getsUserCalendarItem = function(id){
	return new Promise(function(resolve, reject){
		var query = {"owner_id":new makeObject(id)};
		console.log(query);
		CalendarIndividual.findOne(query).populate("days_id_my days_id_member days_id_group").exec(function(err, result){
			console.log("this");
			console.log(err);
			if(err){
				reject(err);
			}else{
				console.log(result);
				var calendar = {
				  "myEvents": result.days_id_my,
				  "memberEvents": result.days_id_member,
				  "groupEvents": result.days_id_group
				}
				resolve(calendar);
			}
		});
	});
}



var getUser = function(id){
	return new Promise(function(resolve, reject){
		var query = {"_id":new makeObject(id)};
		Users.find(query, function(err, result){
			if(err){
				reject(err);
			}else if(result.length != 1){
				reject("error any matches")
			}else{
				var userInfo = result[0];
				resolve(userInfo);
			}
		})
	});
}

function addEventUsers(item, id){
	return new Promise(function(resolve, reject){
		Days.create(item['newEvent'], function(err, days){
			console.log(days);
			if(item['type'] == 1){
				CalendarIndividual.update({"owner_id":new makeObject(id)}, {"$push":{"days_id_my":days._id, "days_id_all":days._id}}, function(err, result){
					console.log(result);
					console.log(err);
				});
			}else{
				CalendarIndividual.update({"owner_id":new makeObject(id)}, {"$push":{"days_id_my":days._id}}, function(err, result){
					console.log(result);
					console.log(err);
				})
			}
			getUser(id).then(function(info){
				resolve({"days":days, "user":info});
			});
		});
	})
}

var noticeInviteGroup = function(inviteUser, groupName, groupId, name, id){
	var query = [];
	for(var i = 0; inviteUser.length > i; i++){
		query.push({"_id":new makeObject(inviteUser[i]),group:{$nin:[groupId]}});
	}
	Users.update({"$or":query}, {"$push":{"noticeGroupInvite":{"groupName":groupName, "groupId":groupId, "date":new Date(), "inviteFromId":id, "inviteFromName":name}}}, {multi:true},function(err, result){
		console.log(err);
		console.log(result);
	});
}


var noticeAddEvent = function(day, user, member, noticeString){

	if(member.length < 1){
		return;
	}else{
		var query = [];
		for(var i = 0; member.length > i ; i++){
			query.push({"_id":new makeObject(member[i])});
		}
		Users.update({"$or":query},{"$push":{"notice":{"name":user.showName,"optionId":day._id,"userId":user._id,"note":noticeString,"type":"friendsAddShare","Date":new Date()}}}, {multi:true},function(err, result){
		});
	}
}

var noticeOffer = function(item){
	var query = {"_id":item["toId"]};
	Users.update(query, {"$push":{"notice":{"name":item["fromName"], "optionId":item["_id"], "userId":item["fromId"], "note":item["aboutDate"], "type":"receiveNewOffer", "Date":new Date()}}}, function(err, result){
		console.log(result);
		console.log(err);
	});
}

var makeObjectIdMember = function(ids){
	return new Promise(function(resolve, reject){
		var member = []
		for(var i = 0; ids.length > i; i++){
			member.push({"owner_id":new makeObject(ids[i])});
		}

		resolve(member);
	})
}

var addDayUsersCalendar = function(item, members, id){
	return new Promise(function(resolve, reject){
		var dayItem = item['newEvent']
		console.log(item);
		console.log(members);
		console.log(id);
		addEventUsers(item, id).then(function(result){
			noticeAddEvent(result['days'],result['user'],members, dayItem['description']['eventTimeString']);
			var dayId = result["days"]['_id'];
			if(members.length > 0){
				for(var i = 0, len = members.length ; len > i; i++){
					members[i] = {"owner_id":new makeObject(members[i])};
				}
				console.log(members);
				CalendarIndividual.update({"$or":members}, {"$push":{"days_id_member":dayId}}, {multi:true},function(err,result){
					console.log(err);
					console.log(result);
				});
			}
			resolve(result["days"]);
		})
	})
}
