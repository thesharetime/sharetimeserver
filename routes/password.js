var express = require("express");
var Promise = require("bluebird");
var model = require("../model/database");
var router = express.Router();

router.get("/send", function(req, res, next){
	res.render("password", {csrfToken:req.csrfToken()});
});

router.get("/reflesh/:id", function(req, res){
	var id = req.params.id;
	model.checkSumRefreshPassword(id).then(function(){
		res.render("passwordReflesh", {"userValue":req.params.id});
	}).catch(function(){
		res.render("error");
	});
});

module.exports = router;
