var express = require('express');
var Promise = require('bluebird');
var router = express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('login', {csrfToken:req.csrfToken()});
});

router.get('/loginfailed', function(req, res, next){
	if(req.session.loginfail){
		delete req.session.loginfail;
		var messages = {"errors":"ログイン失敗",
						"anyScript":'errors();'};
		res.render('login', messages);
	}else{
		res.redirect("/");
	}
})

module.exports = router;
