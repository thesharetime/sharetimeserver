(function(){
	var tmpEvent = false;
	var nowMonthIs = 0;
	var isTouch = true;
	var deviceHeight = $(window).height() - $("#navbarHeader").height();
	var deviceWidth = $(window).width();
	$("body").css({
		"padding-top":$("#navbarHeader").height()
	});

	$("#MainContents").on("click touchend", function(){
		if((isBreakPoint("xs") || isBreakPoint("sm")) && $("#navbarMenuItems").is(":visible")){
			$("#navbarMenuItems").collapse('hide');
		}
	});

	$("#MainContents").css("height", $(window).height() - $("#navbarHeader").height());		

	$("#calendar").fullCalendar({
		header: {
			left: 'prev',
			center: 'title',
			right: 'next'
		},
		eventSources: [
		{
			events: calendar.myEvents,
			color: "#008fbd"
		},
		{
			events: calendar.memberEvents,
			color: "#FFD700"
		},
		{
			events: calendar.groupEvents,
			color: "#44A728"
		}
		],
		views:{
			month:{
				titleFormat: "M月",
				eventLimit:3
			}
		},
		lang:"ja",
		aspectRatio:($(window).height() - $("#navbarHeader").height())/$(window).width(),
		height:$(window).height() - $("#navbarHeader").height(),
		dayClick: function(date, jsEvent, view){
			if(tmpEvent){
				return false;
			}else{
				if(isBreakPoint('xs') || isBreakPoint('sm')){
					mobileIndicatorShow();
					showMobileIndicatorComponents("day");
					if(!$(jsEvent.target).hasClass("fc-past")){
						prepareCalendarList(date);
					}else{
						prepareCalendarList(date, "past");
					}
				}else{
					if(!$(jsEvent.target).hasClass("fc-past")){
						prepareCalendarList(date, null, dialogShow);
					}else{
						prepareCalendarList(date, "past", dialogShow);
					}
				}

				
			}
		},
		eventClick:function(event ,jsEvent, view){
			var date = moment(event["description"]["dayString"], "YYYY/MM/DD")
			if(isBreakPoint('xs') || isBreakPoint('sm')){
				mobileIndicatorShow();
				showMobileIndicatorComponents("day");
				if(!$(jsEvent.target).hasClass("fc-past")){
					prepareCalendarList(date);
				}else{
					prepareCalendarList(date, "past");
				}
			}else{
				if(!$(jsEvent.target).hasClass("fc-past")){
					prepareCalendarList(date, null, dialogShow);
				}else{
					prepareCalendarList(date, "past", dialogShow);
				}
			}
			return false;
		}
	});
	$(".fc-prev-button").hide();

	$(".myProfileUserName").html(user["user_name"]);
	if(user["familyName"].length > 0){
		$(".familyName").val(user["familyName"]).html(user["familyName"]);
	}else{
		$("#myProfileFamilyName").html("未設定");
	}

	if(user["firstName"].length > 0){
		$(".firstName").val(user["firstName"]).html(user["firstName"]);
	}else{
		$("#myProfileFirstName").html("未設定");
	}

	if(user["familyName_hurigana"].length > 0){
		$(".familyName_hurigana").val(user["familyName_hurigana"]);
	}

	if(user["firstName_hurigana"].length > 0){
		$(".firstName_hurigana").val(user["firstName_hurigana"]);
	}

	if(user["showName"] != user["user_name"]){
		$(".showName").val(user["showName"]).html(user["showName"]);
	}else{
		$("#myProfileShowName").html("未設定");
	}

	if(user["comment"].length > 0){
		var comment = user["comment"].replace(/\n/g, "<br>");
		$("#myComment").html(comment);
		$("#profileComment").val(user["comment"]);
	}else{
		$("#myComment").html("未設定");
	}

	if(user["birthDay"].length > 0){
		var birthDays = user["birthDay"].split("-");
		$("#myProfileBirthday").html(birthDays[0] + "年" + birthDays[1] + "月" + birthDays[2]);
		$(".profileYears").val([birthDays[0]]);
		$("#yearSelect").html(birthDays[0] + "<span class='caret'></span>");
		$(".profileMonths").val([birthDays[1]]);
		$("#monthSelect").html(birthDays[1] + "<span class='caret'></span>");
		$(".profileDays").val([birthDays[2]]);
		$("#daySelect").html(birthDays[2] + "<span class='caret'></span>");
	}else{
		$("#myProfileBirthday").html("未設定");
	}

	if(Number(user.accessTime < 5)){
		var script = document.createElement("script");
		script.src = "/javascripts/lib/tutorial.js";
		document.body.appendChild(script);
	}

	if($(".device-sm, .device-xs").is(":visible")){
		$(".fc-toolbar").hide();
		$("#calendar").fullCalendar("option", "height", ($(window).height() - $("#navbarHeader").height())+$(".fc-toolbar").height());
		$("#prevMonth").css("left", -deviceWidth).show();
		$("#nextMonth").css("right", -deviceWidth).show();
	}

	
	var script = document.createElement("script");
	script.src = "/javascripts/lib/index.js";
	document.body.appendChild(script);

	function isBreakPoint(alias){
		return $(".device-" + alias).is(":visible");
	}

	moment.locale('ja', {
		weekdays:["日曜日","月曜日","火曜日","水曜日","木曜日","金曜日","土曜日"],
		weekdaysShort:["日","月","火","水","木","金","土"]
	});	

	function toggleBootStrap(ele){
		if(ele.hasClass("hidden")){
			ele.removeClass("hidden");
			ele.addClass("show")
		}else{
			ele.removeClass("show");
			ele.addClass("hidden")
		}
	}

	allList();


	$("#navbarNoticeBack").on("touchend click", function(){
		$("#myNotices").hide();
		$("#navbarList").show();
		return false;	
	});

	$("#navbarMenuGroupList").on("touchend click", function(){
		if($("#mobileIndicator").is(":hidden")){
			mobileIndicatorShow();
		}

		showMobileIndicatorComponents("group");
		return false;
	})

	$("#navbarMenuFriendList").on("touchend click",function(){
		if($("#mobileIndicator").is(":hidden")){
			mobileIndicatorShow();
		}

		showMobileIndicatorComponents("friends");
		return false;
	})


	$(".navbarMenu").on("click touchend", function(){
		if(isBreakPoint('xs')){
			$("#navbarMenuItems").collapse('hide');
		}
		return false;
	});	

	$("#memEve").on("touchend click",function(){
		$("#friendsChoice").show();
		$("#groupChoice").hide();
		return false;
	});

	$(".makeNewGroupBtn").on("touchend click",function(){
		makeNewGroup();
		return false;
	});

	$("#showAllEventAndShare").on("click touchend", function(){
		prepareCalendarList(calendar["myEvents"], calendar["memberEvents"], calendar["groupEvents"]);
	});

	$(".backContents").on("touchend click",function(){
		mobileIndicatorHide();
		return false;
	});


	$("#MakeShareTime").on("touchend click", function(){
		var today = moment($("#mobileIndicatorDay").data("today"), "YYYY/MM/DD");
		makeShareTime(today);
	});

	$("#groups").on("touchend click",function(){
		$("#friendsChoice").hide();
		$("#groupChoice").show();
	});


	$(".searchStart").on("touchend click",function(event){
		var seek = $.trim($(this).closest('div').children('input').val());
		if(seek.length > 0){
			var input;
			if(seek.indexOf("@") >= 0 && seek.length > 1){
				seek = seek.replace("　", " ");
				seek = seek.replace("@", "");
				var seekFor = seek.split(" ");
				input = {"user_name":seekFor[0]}
			}else{
				seek = seek.replace("　", " ");
				var seekFor = seek.split(" ");
				input = {};
				for(var i = 0; seekFor.length > i; i++){
					input["q" + i] = seekFor[i];
				}
			}

			searchAjax(input);
		}
	});

	$("#calendar").on("touchend click",".fc-prev-button",function(){
		switch (--nowMonthIs){
			case 0:
				$(".fc-prev-button").hide();
				$(".fc-next-button").show();
				break;
			case 1:
				$(".fc-prev-button").show();
				$(".fc-next-button").show();
				break;
			case 2:
				$(".fc-prev-button").show();
				$(".fc-next-button").hide();
				break;
			default:
				calendarRerender();
				break;
		}
	
		$("#calendar").fullCalendar('rerenderEvent');
	});
	$("#calendar").on("touchend click",".fc-next-button",function(){
		switch (++nowMonthIs){
			case 0:
				$(".fc-prev-button").hide();
				$(".fc-next-button").show();
				break;
			case 1:
				$(".fc-prev-button").show();
				$(".fc-next-button").show();
				break;
			case 2:
				$(".fc-prev-button").show();
				$(".fc-next-button").hide();
				break;
			default:
				calendarRerender();
				break;
		}			
		$("#calendar").fullCalendar('rerenderEvent');
	});


	$("#editConfirm").on("touchend click",function(){
		function trimSpaces(str){
			return str.replace(/ |　/g, "");
		}

		var comment = trimSpaces($("#profileComment").val());
		var showName = trimSpaces($("#showName").val());
		var familyName = trimSpaces($("#familyName").val());
		var firstName = trimSpaces($("#firstName").val());
		var familyName_hurigana = trimSpaces($("#familyName_hurigana").val());
		var firstName_hurigana = trimSpaces($("#firstName_hurigana").val());

		if((!$(".profileYears:checked").val() || !$(".profileMonths:checked").val() || !$(".profileDays:checked").val()) && !(!$(".profileYears:checked").val() && !$(".profileMonths:checked").val() && !$(".profileDays:checked").val())){
			showError({title:"誕生日が正しく選択されていません", message:"誕生日を正しく選択してください"});
		}else if((familyName && !firstName) || (!familyName && firstName)){
			showError({title:"姓・名前の片方しか入力されていません。", message:"姓・名前両方入力してください"});
		}else if((familyName_hurigana && !firstName_hurigana)||(!familyName_hurigana && firstName_hurigana)){
			showError({title:"ふりがなが片方しか入力されていません。", message:"ふりがなを両方入力してください"});
		}else{
			if($("#showName").val().length == 0){
				var showName = $("#familyName").val() + $("#firstName").val();
			}

			var data = {
				"sendDataProfileComment":comment,
				"sendDataProfileShowName":showName,
				"sendDataProfileFamilyName":familyName,
				"sendDataProfileFirstName":firstName,
				"sendDataProfileFamilyName_hurigana":familyName_hurigana,
				"sendDataProfileFirstName_hurigana":firstName_hurigana,
				"sendDataProfileBirthDay":$(".profileYears:checked").val() + "-" + $(".profileMonths:checked").val() + "-" + $(".profileDays:checked").val()
			}

			$.ajax({
				type:"POST",
				url:"/modeDBactionsloggedInIndex/editmyprofile",
				data:{
					"data":data
				},
				dataType:"json",
				success:function(json){
					for(key in json){
						user[key] = json[key];
					}

					if(user.familyName){
						$(".familyName").val(user["familyName"]).html(user["familyName"]);
					}else{
						$("#myProfileFamilyName").html("未設定");
					}

					if(user.firstName){
						$(".firstName").val(user["firstName"]).html(user["firstName"]);
					}else{
						$("#myProfileFirstName").html("未設定");
					}

					if(user.familyName_hurigana){
						$(".familyName_hurigana").val(user["familyName_hurigana"]);
					}
					if(user.firstName_hurigana){
						$(".firstName_hurigana").val(user["firstName_hurigana"]);
					}

					if($("#showName").val() != user["user_name"]){
						$(".showName").val(user["showName"]).html(user["showName"]);
					}else{
						$("#myProfileNickName").html("未設定");
					}
					if(user.comment){
						var comment = user["comment"].replace(/\n/g, "<br>");
						$("#myComment").html(comment);
						$("#profileComment").val(user["comment"]);
					}else{
						$("#myProfileComment").html("未設定");
					}

					if(user.birthDay){
						var birthDays = user["birthDay"].split("-");
						$("#myProfileBirthday").html(birthDays[0] + "年" + birthDays[1] + "月" + birthDays[2] + "日");
						$(".profileYears").val([birthDays[0]]);
						$("#yearSelect").html(birthDays[0] + "<span class='caret'></span>");
						$(".profileMonths").val([birthDays[1]]);
						$("#monthSelect").html(birthDays[1] + "<span class='caret'></span>");
						$(".profileDays").val([birthDays[2]]);
						$("#daySelect").html(birthDays[2] + "<span class='caret'></span>");
					}else{
						$("#myProfileBirthday").html("未設定");
					}

					$("#myProfile").show();
					$("#editProfile").hide();
				}
			});

		}
	});

	$("#editCancel").on("touchend click",function(){
		$("#editProfile").hide();
		$("#myProfile").show();
		if(user["familyName"].length > 0){
			$("#familyName").val(user["familyName"]);
		}
		if(user["firstName"].length > 0){
			$("#firstName").val(user["firstName"]);
		}
		if(user["familyName_hurigana"].length > 0){
			$("#familyName_hurigana").val(user["familyName_hurigana"]);
		}
		if(user["firstName_hurigana"].length > 0){
			$("#firstName_hurigana").val(user["firstName_hurigana"]);
		}
		if(user["showName"].length > 0 && user["showName"] != user["user_name"]){
			$("#showName").val(user["showName"])
		}
		if(user["comment"].length > 0){
			$("#profileComment").val(user["comment"]);
		}
	});	

	$("#navbarMenuEditProfile").on("touchend click",function(){
		if($("#mobileIndicator").is(":visible")){
			mobileIndicatorHide();
		}
		$("#calendar").hide();
		$("#myProfile").show();
	});

	$("#mobileNavbarMenuNotice").on("touchend click", function(){
		$("#myNotices").css("height", $("#navbarList").height()).show();
		$("#navbarList").hide();
		return false;
	});	

	$("#navbarMenuMyCalendar, .navbar-brand").on("click touchend", function(){
		if($("#mobileIndicator").is(":visible")){
			mobileIndicatorHide();
		}

		if($("#calendar").is(":hidden")){
			$("#calendar").show();
			$("#editProfile").hide();
			$("#myProfile").hide();
		}

		calendarRerender();
	});	

	$("#navbarMenuItems").on("hidden.bs.collapse", function(){
		$("#navbarList").show();
		$("#myNotices").hide();
	})

	$(".resultBackBtn").on("touchend click", function(){
		$(".indicatorSearch").hide();
		$(".memberListClass").show();
	});

	$("#myProfileEdit").on("touchend click", function(){
		$("#editProfile").show();
		$("#myProfile").hide();

	});

	$("#myProfileCancel").on("touchend click", function(){
		$("#myProfile").hide();
		$("#calendar").show();
	});

	$("body").css({
		"padding-top":$("#navbarHeader").height()
	});

	$("#nowMonthIs").html(moment().format("MMMM"));
	$("#nextMonthIs").html(moment().add(1, "M").format("MMMM"));
	$("#prevMonthIs").html(moment().add(-1, "M").format("MMMM"));

	if(isBreakPoint('xs') || isBreakPoint('sm')){
		$(".thumbnails").removeAttr("style");
		$("#prevMonth").removeAttr("style").css("left", -deviceWidth);
		$("#nextMonth").removeAttr("style").css("right", -deviceWidth);
	}else{
		$("#prevMonth").removeAttr("style").css("left", -deviceWidth).hide();
		$("#nextMonth").removeAttr("style").css("right", -deviceWidth).hide();
	}

	(function(){
		var originX = 0;
		var originY = 0;
		var nextOrPrev;

		$("#calendar").on({
		"touchmove":function(event){
			tmpEvent = true;
			var moveX = event.originalEvent.touches[0].clientX - originX;
			var moveY = event.originalEvent.touches[0].clientY - originY;
			if(Math.abs(moveX) > Math.abs(moveY)){
				event.preventDefault();
				if(moveX > 0 && moveX <= deviceWidth/3 && nowMonthIs > 0){
					$("#prevMonth").css("left", (-deviceWidth/3) + moveX);
				}else if(moveX < 0 && -(moveX) <= deviceWidth/3 && nowMonthIs < 2){
					$("#nextMonth").css("right", (-deviceWidth/3) - moveX);
				}

				if(moveX >= deviceWidth/3 && nowMonthIs > 0){
					nextOrPrev = "prev";
				}else if(moveX <= -deviceWidth/3 && nowMonthIs < 2){
					nextOrPrev = "next";
				}
			}
		},
		"touchstart":function(event){
			originX = event.originalEvent.touches[0].clientX;
			originY = event.originalEvent.touches[0].clientY;
			$("#nextMonth").css("right", -deviceWidth/3);
			$("#prevMonth").css("left", -deviceWidth/3);
		},
		"touchend":function(event){
			if(nextOrPrev == "next"){
				$(".fc-next-button").trigger("click");
				$("#nowMonthIs").html(moment().add(nowMonthIs, "M").format("MMMM"));
				$("#nextMonthIs").html(moment().add(1 + nowMonthIs, "M").format("MMMM"));
				$("#prevMonthIs").html(moment().add(nowMonthIs - 1, "M").format("MMMM"));

				nextOrPrev = null;
			}else if(nextOrPrev == "prev"){
				$(".fc-prev-button").trigger("click");
				$("#nowMonthIs").html(moment().add(nowMonthIs, "M").format("MMMM"));
				$("#nextMonthIs").html(moment().add(1 + nowMonthIs, "M").format("MMMM"));
				$("#prevMonthIs").html(moment().add(nowMonthIs - 1, "M").format("MMMM"));

				nextOrPrev = null;
			}else{

			}
			setTimeout(function(){
				tmpEvent = false;
			}, 250);
			$("#nextMonth").css("right", -deviceWidth);
			$("#prevMonth").css("left", -deviceWidth);
		}
		});
	}());


	function showMobileIndicatorComponents(targetComp){
		var nowVisible = $(".mobileIndicatorItems:visible");

		switch (targetComp){
			case 'day':
				nowVisible.addClass("hidden").removeClass("show");
				$("#mobileDayDetail").addClass("show").removeClass("hidden");
				break;
			case 'group':
				nowVisible.addClass("hidden").removeClass("show");
				$("#mobileGroupComponents").addClass("show").removeClass("hidden");
				break;
			case 'friends':
				nowVisible.addClass("hidden").removeClass("show");
				$("#mobileMemberComponents").addClass("show").removeClass("hidden");
				break;
			default:
				break;
		}
	}


	var waitForFinalEvent=function(){
		var b={};
		return function(c,d,a){
			a||(a="resizableFunction");
			b[a]&&clearTimeout(b[a]);
			b[a]=setTimeout(c,d)
		}
	}();

	$(window).resize(function () {
		deviceHeight = $(window).height() - $("#navbarHeader").height();
		deviceWidth = $(window).width();

		$("body").css({
			"padding-top":$("#navbarHeader").height()
		});
		waitForFinalEvent(function(){
			$("#calendar").fullCalendar("option", "height", $(window).height());
			if($("#mobileIndicator").is(":hidden")){
				if(isBreakPoint("xs") || isBreakPoint("sm")){
					$(".thumbnails").removeAttr("style");
					$(".fc-toolbar").hide();
				}else{
					$(".thumbnails").attr("style", "padding:0;margin-left:0;")
					$(".fc-toolbar").show();
				}
			}else{
				mobileIndicatorShow();
			}
		}, 300, (new Date()).getTime())
	});


	function inviteFriends(groupID, groupName, groupMember){

		var tmpGroupMember = [];

		for(var i = 0; groupMember.length > i; i++){
			tmpGroupMember.push(groupMember[i]["_id"]);
		}


		var noRegister = [];

		for(var i = 0; user["friends"].length > i; i++){
			if(tmpGroupMember.indexOf(user["friends"][i]["_id"]) < 0){
				noRegister.push(user["friends"][i]);
			}
		}

		if(noRegister.length < 1){
			var dialog = $("<div style='width:100%;' class='modal'></div>");

			var content = (function(){/*
				<div class="modal-dialog" style="padding-top:100px;">
					<div class="modal-content">
						<div class="modal-body">
							加入していない友人はいません
						</div>
						<div class="modal-footer">
							<button class="btn btn-default" data-dismiss="modal">閉じる</button>
						</div>
					</div>
				</div>
			*/}).toString().match(/\/\*([^]*)\*\//)[1];

			dialog.append(content);

			dialog.on("hidden.bs.modal", function(event){
				dialog.remove();
			});
			dialog.modal();
		}else{
			var dialog = $("<div style='width:100%;height:100%' class='modal'></div>");
			var members = "";

			var content = (function(){/*
				<div class="modal-dialog" style="padding-top:200px;">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">##GroupNameに招待</h4>
						</div>
						<div class="modal-body">
							<select id="inviteFriendsGroup" multiple>
								##InviteFriends
							</select>
						</div>
						<div class="modal-footer">
							<button class="btn btn-primary" id="inviteConfirm">##GroupNameに招待</button><button class="btn btn-default" id="inviteGroupFriendsCancel" data-dismiss="modal">キャンセル</button>
						</div>
					</div>
				</div>
			*/}).toString().match(/\/\*([^]*)\*\//)[1];


			for(var i = 0; noRegister.length > i; i++){
				members += "<option value='"+ noRegister[i]["_id"] +"'>"+ noRegister[i]["showName"] +"</option>"
			}

			content = content.replace(/##GroupName/g, groupName);
			content = content.replace("##InviteFriends", members);

			dialog.append(content);

			dialog.on("hidden.bs.modal", function(event){
				dialog.remove();
			});

			dialog.modal();

			$("#inviteFriendsGroup").selecter({mobile:true});

			$("#inviteConfirm").on("touchend click",function(){
				var inviteFriends = $("#inviteFriendsGroup").val();

				var sendData = {
					"inviteUser":inviteFriends,
					"name":user["showName"],
					"groupID":groupID,
					"groupName":groupName
				};

				$.ajax({
					url:"/modeDBactionsloggedInIndex/inviteFriendsToGroup",
					type:"POST",
					data:sendData,
					dataType:"json",
					success:function(json){
						dialog.modal("hide");
					}
				});
			});
		}

	}

	function makeNewGroup(){
		var myFriends = user["friends"];
		var dialog = $("<div class='modal'></div>");
		var members = "";

		for(var i = 0; myFriends.length > i; i++){
			members += "<option class='inviteGroupCheck' value='"+ myFriends[i]["_id"] +"'>"+ myFriends[i]["showName"] +"</option>"
		}


		var content = (function(){/*
			<div class="modal-dialog" style="padding-top:100px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">グループ作成</h4>
					</div>
					<div class="modal-body">
						<div class="row" style="margin:10px auto;">
							<label for="groupName">グループ名</label>
							<input id="groupName" placeholder="グループ名：" type="text" class="form-control">
						</div>
						<div class="row" style="margin:10px auto;">
							<label for="groupComment">グループ紹介文</label>
							<textarea id="groupComment" placeholder="グループ紹介文：" rows="5" class="form-control"></textarea>
						</div>
						<div class="row text-center" style="margin:0 auto;">
							<label for="thisSelect">友達を招待</label>
							<div class="btn-group btn-block">
								<select multiple="multiple" id="thisSelect">
									<option value="all">全員</option>
									##InviteFriends
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="makeGroupComfirmBtn">グループ作成</button><button class="btn btn-default" data-dismiss="modal">キャンセル</button>
					</div>
				</div>
			</div>
		*/}).toString().match(/\/\*([^]*)\*\//)[1]

		content = content.replace("##InviteFriends", members);
		dialog.append(content);

		dialog.on("hidden.bs.modal", function(event){
			dialog.remove();
		});
		dialog.modal();

		$("#thisSelect").selecter({
			mobile:true
		});

		var state;
		var stateNode;
		var stateVal;

		$("#thisSelect").on("change", function(event){
			if($(this).val() && $(this).val().indexOf("all") >= 0){
				stateVal = $(this).val();
				stateNode = $(".selecter-item.selected");
				$(".selecter-item").addClass("selected");
				state = true;
			}else if(state){
				state = false;
				stateVal.splice(stateVal.indexOf("all"), 1);
				$(".selecter-item").removeClass("selected");
				stateNode.addClass("selected");
				$(this).val(stateVal);
				$($(".selecter-item")[$(".selecter-item").data("value").indexOf("all")]).removeClass("selected");
			}
		});



		$("#makeGroupComfirmBtn").on("touchend click",function(){
			if(!$("#groupName").val()){
				showError({title:"グループの名前が入力されていません", message:"グループ名を入力してください"});
			}else if(!$("#groupComment").val()){
				showError({title:"グループの説明が入力されていません", message:"グループの説明を入力してください"});
			}else{
				var inviteUser = [];
				var inviteFriends = $("#thisSelect").val();

				if(!inviteFriends){
					;
				}else if(inviteFriends.indexOf("all") < 0){
					for(var i = 0, len = inviteFriends.length; len > i; i++){
						inviteUser.push(inviteFriends[i]);
					}
				}else{
					inviteFriends = user["friends"];
					for(var i = 0, len = inviteFriends.length; len > i; i++){
						inviteUser.push(inviteFriends[i]["_id"]);
					}
				}

				var sendData = {
					"comment":$("#groupComment").val(),
					"groupName":$("#groupName").val(),
					"inviteUser":inviteUser,
					"name":user["showName"]
				};


				$.ajax({
					url:"/modeDBactionsloggedInIndex/makeGroup",
					type:"POST",
					data:sendData,
					success:function(json){
						dialog.modal("hide");
						user["group"].push(json);
						groupList();
					}
				});
			}

			return false;
		});
	}



	function removeRequestFriend(id){
		$.ajax({
			type:"POST",
			url:"/modeDBactionsloggedInIndex/removerequestfriend",
			data:{"id":id},
			dataType:"json",
			success:function(newRequest){
				user.request = newRequest;
			}
		})
	}

	var requestFriend = function(id){
		$.ajax({
			type:"POST",
			url:"/modeDBactionsloggedInIndex/requestfrienduser",
			data:{"id":id, "name":user['showName']},
			dataType:"json",
			success:function(json){

			}
		})
	}

	function attendOrAbsence(thisIndex){
		var inviteItem = user.noticeGroupEvent[thisIndex];
		var groupID = inviteItem["groupId"];
		var eventID = inviteItem["eventId"];

		var dialog = $("<div class='modal'</div>");

		var eventDetail;

		var content = (function(){/*
			<div class="modal-dialog" style="padding-top:100px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">イベント詳細</h4>
					</div>
					<div class="modal-body">
						<div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">イベント名：<h4>##EventTitle</h4></div>
						<div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">提案者： <h4>##EventSuggestionBy</h4></div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">日付： <h4>##EventDay</h4></div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">時間： <h4>##EventTime</h4></div>
						詳細：
						<div class="panel panel-default">
							<div class="panel-body" style="height:auto;max-height:150px;overflow-x:hidden;padding:10px;">
							##EventContent
							</div>
						</div>
						出席者：
						<ul class="list-group" style="height:auto;max-height:150px;overflow-x:hidden;">
							##EventAttendNow
						</ul>
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary" id="groupEventAttendBtn" type="button">出席</button><button id="groupEventAbsenceBtn" class="btn btn-danger" type="button">欠席</button><button type="button" data-dismiss="modal" class="btn btn-default">キャンセル</button>
					</div>
				</div>
			</div>
		*/}).toString().match(/\/\*([^]*)\*\//)[1];

		dialog.on("hidden.bs.modal", function(){
			dialog.remove();
			checkNotice();
		});

		dialog.on("show.bs.modal", function(){
			$.ajax({
				url:"/modeDBactionsloggedInIndex/getEventDetail",
				data:{id:eventID},
				dataType:"json",
				type:"get",
				success:function(events){
					var times = events["description"]["eventTimeString"].split(" ");
					content = content.replace("##EventTitle", events["title"]);
					content = content.replace(/##EventContent/g, events["description"]["comment"].replace(/\n/g, "<br>"));
					content = content.replace("##EventSuggestionBy", events["description"]["suggestionBy"]["showName"]);
					content = content.replace("##EventDay", times[0]);
					content = content.replace("##EventTime", times[1]);
					var mem = events.description.attend;
					var member = "";
					for(var i = 0; mem.length > i; i++){
						member += "<li class='list-group-item'>"+ mem[i]["showName"] + "</li>";
					}
					content = content.replace("##EventAttendNow", member);
					dialog.append(content);
					$("#groupEventAttendBtn").on("click touchend",function(){
						var data = {
							"eventID":eventID,
							"groupID":groupID,
							"noticeId":inviteItem._id,
							"name":user["showName"]
						};

						$.ajax({
							type:"POST",
							data:data,
							dataType:"json",
							url:"/modeDBactionsloggedInIndex/attendGroupEvent",
							success:function(json){
								user.noticeGroupEvent.splice(thisIndex, 1);
								noticeList("groupEvent");
								dialog.modal("hide");
							}
						});

						return false;
					});

					$("#groupEventAbsenceBtn").on("touchend click",function(){
						$.ajax({
							type:"POST",
							dataType:"json",
							data:{
								"eventID":eventID,
								"noticeId":inviteItem._id
							},
							url:"/modeDBactionsloggedInIndex/absenceGroupEvent",
							success:function(json){
								user.noticeGroupEvent.splice(thisIndex, 1);
								noticeList("groupEvent");
								dialog.modal("hide");
							}
						});
					});					
				}
			});
		});

		dialog.modal();

		if(isBreakPoint("xs") || isBreakPoint("sm")){
			$(".modal-dialog").css("padding-top", "0");
		}
	}



	function showInviteGroup(thisIndex){
		var groupNotice = user.noticeGroupInvite[thisIndex];
		var id = groupNotice["groupId"];
		var name = groupNotice["groupName"];
		var fromName = groupNotice["inviteFromName"];

		var dialog = $("<div style='width:100%;' class='modal'></div>");


		var content = (function(){/*
			<div class="modal-dialog modal-md" style="padding-top:100px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">##GroupNameへ招待</h4>
					</div>
					<div class="modal-body" style="height:auto;max-height:450px;">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<div>グループ名：<span id="thisGroupName" class="h4"></span></div>
								<div>リーダー名： <span id="thisGroupLeader" class="h4"></span></div>
								<div>招待主： <span id="invitedBy" class="h4"></span></div>
								コメント：
								<div class="panel panel-default">
									<div id="thisGroupComment" style="max-height:150px;height:150px;overflow-x:hidden;" class="panel-body"></div>
								</div>
								メンバー：
								<ul id="thisGroupMember" style="max-height:200px;height:auto;overflow-x:hidden;" class="list-group"></ul>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="joinGroupBtn">加入</button><button type="button" class="btn btn-danger" id="rejectGroupBtn">拒否</button><button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
					</div>
				</div>
			</div>
		*/}).toString().match(/\/\*([^]*)\*\//)[1];


		content = content.replace("##GroupName", name);

		dialog.append(content);
		dialog.on("hidden.bs.modal", function(){
			dialog.remove();
			checkNotice();
		});

		dialog.on("show.bs.modal", function(){
			$.ajax({
				type:"GET",
				url:"/modeDBactionsloggedInIndex/getGroupInfo",
				data:{
					"id":id
				},
				dataType:"json",
				success:function(group){
					var member = group["member"];
					$("#thisGroupName").html(group["groupName"]);
					$("#thisGroupLeader").html(group.leader.showName);
					$("#invitedBy").html(fromName);
					$("#thisGroupComment").html(group["comment"].replace(/\n/g, "<br>"));
					for(var i = 0; member.length > i; i++){
						$("#thisGroupMember").append("<li class='list-group-item'>"+member[i]["showName"]+"</li>")
					}
				}
			});
		});

		dialog.modal();

		if(isBreakPoint("xs") || isBreakPoint("sm")){
			$(".modal-dialog").css("padding-top", "0");
		}

		$("#joinGroupBtn").on("touchend click" ,function(){
			$.ajax({
				type:"POST",
				url:"/modeDBactionsloggedInIndex/confirmGroupInvite",
				data:{"id":id, "groupName":name, "user_name":user["showName"]},
				dataType:"json",
				success:function(json){
					if(json["groupDays"].length > 0){
						calendar['groupEvents'] = calendar['groupEvents'].concat(json["groupDays"]);
						calendarRerender();
					}
					if(json["group"]){
						user["group"].push(json["group"]);
						groupList();
					}
					user.noticeGroupInvite.splice(thisIndex, 1);
					noticeList("groupInvite");
					dialog.modal("hide");
				}
			});

			return false;
		});

		$("#rejectGroupBtn").on("click touchend",function(){
			node.remove();
			dialog.modal("hide");
			$.ajax({
				type:"POST",
				url:"/modeDBactionsloggedInIndex/rejectGroupInvite",
				data:{"id":id},
				dataType:"json",
				success:function(json){
					user.noticeGroupInvite.splice(thisIndex, 1);
					noticeList("groupInvite");
					dialog.modal("hide");
				}
			});

			return false;
		});

	}


	function showRequestListDialog(thisIndex){
		var friendItem = user.noticeFriends[thisIndex];
		var id = friendItem["_id"];
		var name = friendItem["name"]


		var dialog = $("<div class='modal'></div>");

		var content = (function(){/*
			<div class="modal-dialog" style="padding-top:100px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">　</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4 col-lg-4 col-sm-6 col-xs-6">
								<div class="thumbnail">
									<img src="/images/people.png" class="img-rounded">
								</div>
							</div>
							<div class="col-md-8 col-lg-8">
								<p>生年月日：<span id="requestBirthday"></span></p>
								<p>ユーザーネーム：<span id="requestUserShowName"></span></p>
								<p>名前：<span id="requestFullName"></span></p>
								<div class="col-xs-12 col-sm-12">
									<p>自己紹介：</p>
									<div class="panel panel-default">
										<div id="requestComment" class="panel-body" style="height:100px;overflow-x:hidden;"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<div class="text-center">共通の友達</div>
									<ul id="requestCommonFriends" class="list-group" style="height:125px;overflow-x:hidden;"></ul>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<div class="text-center">共通のグループ</div>
									<ul id="requestCommonGroup" class="list-group" style="height:125px;overflow-x:hidden;"></ul>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="confirmFriendRequestBtn" class="btn btn-primary">友達になる</button><button type="button" id="rejectFriendRequestBtn" class="btn btn-danger">拒否</button><button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
					</div>
				</div>
			</div>
		*/}).toString().match(/\/\*([^]*)\*\//)[1];

		dialog.append(content);

		dialog.one("hidden.bs.modal", function(){
			dialog.remove();
			checkNotice();
		});

		dialog.one("show.bs.modal", function(){
			$.ajax({
				type:"GET",
				url:"/modeDBactionsloggedInIndex/requestMemberComm",
				data:{
					"id":id
				},
				dataType:"json",
				success:function(json){
					var commonGroup = json['commonGroup'];
					var commonUser = json['commonUser'];
					if(commonGroup.length > 0){
						$.each(commonGroup, function(i, item){
							$("#requestCommonGroup").append("<li class='list-group-item'>"+ item['groupName'] +"</li>");
						})
					}else{
							$("#requestCommonGroup").append("<li class='list-group-item'>共通のグループはありません</li>");
					}
					if(commonUser.length > 0){
						$.each(commonUser, function(i, item){
							$("#requestCommonFriends").append("<li class='list-group-item'>"+ item['user_name'] +"</li>");
						})
					}else{
							$("#requestCommonFriends").append("<li class='list-group-item'>共通の友人はいません</li>");
					}
				}
			});

			$.ajax({
				type:"GET",
				url:"/modeDBactionsloggedInIndex/requestMemberInfo",
				data:{
					"id":id
				},
				dataType:"json",
				success:function(json){
					$("#requestUserShowName").html(json["showName"]);
					$("#requestBirthday").html(json["birthDay"]);
					$("#requestComment").html(json["comment"].replace(/\n/g, "<br>"));
					$("#requestFullName").html(json["familyName"] + "" + json["firstName"]);
				}
			});
		});

		dialog.modal();

		if(isBreakPoint("xs") || isBreakPoint("sm")){
			$(".modal-dialog").css("padding-top", "0");
		}

		$("#confirmFriendRequestBtn").one("click touchend",function(){
			$.ajax({
				type:"POST",
				url:"/modeDBactionsloggedInIndex/acceptRequestFriend",
				data:{"id":id, "user_name":user["user_name"], "friend_name":name},
				dataType:"json",
				success:function(json){
					if(json.calendar.length > 0){
						calendar['memberEvents'] = calendar['memberEvents'].concat(json.calendar);
						calendarRerender();
					}
					user.noticeFriends.splice(thisIndex, 1);
					noticeList("requestFriend");
					user["friends"].push(json.newFriend);
					memberList();
					dialog.modal("hide");
				}
			});

			return false;
		});

		$("#rejectFriendRequestBtn").on("touchend click",function(){
			$.ajax({
				type:"POST",
				url:"/modeDBactionsloggedInIndex/rejectrequestfriend",
				data:{"id":id},
				dataType:"json",
				success:function(noticeFriends){
					user.noticeFriends = noticeFriends;
					noticeList("requestFriend");
					dialog.modal("hide");
				}
			});

			return false;
		});
	}

	function searchAjax(input){
		var searchTemplate = (function(){/*
			<a class="list-group-item clearfix">
				<span class="pull-right">
					<button class="btn btn-lg btn-primary searchRequestList" data-id="##ID"><span class="glyphicon glyphicon-user"></span>詳細</button>
				</span>
				<h4 class="list-group-item-heading">##Name</h4>
			</a>
		*/}).toString().match(/\/\*([^]*)\*\//)[1];

		$(".memberListClass").hide();
		$(".indicatorSearch").show();
		$("#searchLoading").show();

		$.ajax({
			type:"GET",
			url: "/modeDBactionsloggedInIndex/searchMember",
			data:input,
			dataType: 'json',
			success: function(json){
				$("#searchLoading").hide();
				if(json.length > 0){
					$(".searchResult").empty();
					$.each(json, function(i, item){
						var resultItem = searchTemplate.replace("##Name", item["showName"]);
						resultItem = resultItem.replace("##ID", item["_id"]);
						$(".searchResult").append(resultItem);
					});

					$(".searchRequestList").on("touchend click",function(){
						showRequestDialog($(this).data('id'));
					});

					return false;
				}else{
					$(".searchResult").empty().append("<a href='#' class='list-group-item disabled'>検索結果：見つかりませんでした</a>")
				}
			}
		})
	}

	var idIndexOf = function(arr ,id){
		for(var i = 0; arr.length > i; i++){
		
			if(arr[i]["id"] == id){
				return i;
			}
		}
		
		return -1;
	}
	var _idIndexOf = function(arr ,id){
		for(var i = 0; arr.length > i; i++){
		
			if(arr[i]["_id"] == id){
				return i;
			}
		}
		
		return -1;
	}
	
	function showRequestDialog(id){
		var dialog = $('<div class="modal"></div>');
		var content;

		if(isBreakPoint('lg') || isBreakPoint('md')){
			content = (function(){/*
				<div class="modal-dialog" style="padding-top:100px;">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">　</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-4 col-lg-4">
									<div class="thumbnail">
										<img src="/images/people.png" class="img-rounded">
									</div>
								</div>
								<div class="col-md-8 col-lg-8">
									<p>生年月日：<span id="requestBirthday"></span></p>
									<p>ユーザーネーム：<span id="requestUserShowName"></span></p>
									<p>名前：<span id="requestFullName"></span></p>
									<p>自己紹介：</p>
									<div class="panel panel-default">
										<div id="requestComment" class="panel-body" style="height:100px;overflow-x:hidden;"></div>
									</div>
								</div>
								<div class="col-lg-12 col-md-12">
									<div class="col-lg-6 col-md-6">
										<div class="text-center">共通の友達</div>
										<ul id="requestCommonFriends" class="list-group" style="height:125px;overflow-x:hidden;"></ul>
									</div>
									<div class="col-lg-6 col-md-6">
										<div class="text-center">共通のグループ</div>
										<ul id="requestCommonGroup" class="list-group" style="height:125px;overflow-x:hidden;"></ul>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer" id="thisModalBtn">
						</div>
					</div>
				</div>
			*/}).toString().match(/\/\*([^]*)\*\//)[1];
		}else{
			content = (function(){/*
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-sm-6 col-xs-6">
									<div class="thumbnail">
										<img src="/images/people.png" class="img-rounded">
									</div>
								</div>
								<div class="col-sm-6 col-xs-6">
									<p>生年月日：</p><p><span id="requestBirthday"></span></p>
									<p>ユーザーネーム：<span id="requestUserShowName"></span></p>
									<p>名前：<span id="requestFullName"></span></p>
								</div>
								<div class="col-sm-12 col-xs-12">
									<div class="text-center">自己紹介：</div>
									<div class="panel panel-default">
										<div id="requestComment" class="panel-body" style="height:100px;overflow-x:hidden;"></div>
									</div>
									<div class="col-sm-12 col-xs-12">
										<div class="text-center">共通の友達</div>
										<ul id="requestCommonFriends" class="list-group" style="height:125px;overflow-x:hidden;"></ul>
									</div>
									<div class="col-sm-12 col-xs-12">
										<div class="text-center">共通のグループ</div>
										<ul id="requestCommonGroup" class="list-group" style="height:125px;overflow-x:hidden;"></ul>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer" id="thisModalBtn">
						</div>
					</div>
				</div>
			*/}).toString().match(/\/\*([^]*)\*\//)[1];
		}


		var buttonSet = '<button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>'

		dialog.append(content);

		dialog.modal();

		dialog.on("hidden.bs.modal", function(){
			dialog.remove();
		});

		
		if(user['request'].indexOf(id) >= 0){
			$("#thisModalBtn").append('<button type="button" id="requestCancelBtn" class="btn btn-danger">申請を取り消す</button>' + buttonSet);
			$("#requestCancelBtn").on("touchend click",function(){
				removeRequestFriend(id);
				user['request'].splice(user['request'].indexOf(id), 1);
				dialog.modal("hide");

				return false;
			});
		}else if(user["_id"] === id || _idIndexOf(user['friends'], id) >= 0){
			$("#thisModalBtn").append(buttonSet);
		}else{
			$("#thisModalBtn").append('<button type="button" id="requestFriendBtn" class="btn btn-primary">友達になる</button>' + buttonSet);
			$("#requestFriendBtn").on("click touchend",function(){
				user['request'].push(id);
				requestFriend(id);
				dialog.modal("hide");

				return false;
			});
		}

		if(id != user["_id"]){
			$.ajax({
				type:"GET",
				url:"/modeDBactionsloggedInIndex/requestMemberInfo",
				data:{
					"id":id
				},
				dataType:"json",
				success:function(json){
					var friendItems = json.friendItem;
					$("#requestUserShowName").html(friendItems["showName"]);
					$("#requestBirthday").html(friendItems["birthDay"]);
					$("#requestComment").html(friendItems["comment"].replace(/\n/g, "<br>"));
					$("#requestFullName").html(friendItems["familyName"] + "" + friendItems["firstName"]);
					var commonGroup = json.common['commonGroup'];
					var commonUser = json.common['commonUser'];
					if(commonGroup.length > 0){
						$.each(commonGroup, function(i, item){
							$("#requestCommonGroup").append("<li class='list-group-item'>"+ item['groupName'] +"</li>");
						})
					}else{
							$("#requestCommonGroup").append("<li class='list-group-item'>共通のグループはありません</li>");
					}
					if(commonUser.length > 0){
						$.each(commonUser, function(i, item){
							$("#requestCommonFriends").append("<li class='list-group-item'>"+ item['showName'] +"</li>");
						})
					}else{
							$("#requestCommonFriends").append("<li class='list-group-item'>共通の友人はいません</li>");
					}
				}
			});
		}else{
			$("#requestUserName").html(user["user_name"]);
			$("#requestUserShowName").html(user["showName"]);
			$("#requestFullName").html(user["familyName"] + " " + user["firstName"]);
			$("#requestBirthday").html(user["birthDay"]);
			$("#requestComment").html(user["comment"]);
			$.each(user["friends"], function(i, item){
				$("#requestCommonGroup").append("<tr><td data='"+ item['id'] +"'>"+ item['name'] +"</td></tr>");
			});
			$.each(user["group"], function(i, item){
				$("#requestCommonGroup").append("<tr><td data='"+ item['_id'] +"' id='myGroup"+ item["_id"] +"'>"+ item['groupName'] +"</td></tr>");
			})
		}
	}

	function removeShareTimeMobile(myIndex, callback){
		var shareTimeID = calendar["myEvents"][myIndex];

		var dialog = $("<div class='modal'></div>");

		var content = (function(){/*
			<div class="modal-dialog modal-sm" style="padding-top:250px;">
				<div class="modal-content">
					<div class="modal-body">
						シェアを削除しますか？
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" id="removeShareTimeConfirmBtn">削除</button><button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
					</div>
				</div>
			</div>
		*/}).toString().match(/\/\*([^]*)\*\//)[1]

		dialog.append(content);
		dialog.modal();
		$("#removeShareTimeConfirmBtn").on("touchend click",function(){
			$.ajax({
				type:"POST",
				url:"/modeDBactionsloggedInIndex/removeShareTime",
				data:{
					"timeID":shareTimeID["_id"]
				},
				dataType:"json",
				success:function(json){
					calendar["myEvents"].splice(myIndex, 1);
					callback();
					$(".modal").modal("hide");
					calendarRerender();
				}
			});

			return false;
		})

		dialog.on("hidden.bs.modal", function(){
			dialog.remove();
		});
	}

	function makeShareTime(today, reDialog){
		var publicLevel = "";
		var myFriends = user["friends"];

		today.locale('ja', {
			weekdays:["日曜日","月曜日","火曜日","水曜日","木曜日","金曜日","土曜日"],
			weekdaysShort:["日","月","火","水","木","金","土"]
		});

		for(var i = 0; myFriends.length > i; i++){
			publicLevel += "<option class='myMemberList chooseFriendsCheck' value='"+ myFriends[i]["_id"] +"'>"+ myFriends[i]["showName"] +"</option>"
		}

		var dialog = $("<div class='modal' style='z-index:5000;'></div>");


		var content = (function(){/*
			<div class="modal-dialog style="padding-right:auto;padding-left:auto;padding-top:auto;">
				<div class="modal-content col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0;">
					<div class="modal-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div id="eventsMakeDetail" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="row">
								##Today
							</div>
							<div class="row">
								<div class="h3 text-right col-xs-5 col-sm-5 col-md-5 col-lg-5" id="startTimeShare">06:00</div><div class="h3 text-center col-xs-2 col-sm-2 col-md-2 col-lg-2">~</div><div class="h3 col-xs-5 col-sm-5 col-md-5 col-lg-5 text-left" id="endTimeShare">18:00</div>
							</div>
							<div class="row">
								<div id="timeSlider" class="noUiSlider col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
							</div>
							<div style="margin:15px auto;" class="text-center" id="publicLevel">
							公開範囲：
								<select multiple="multiple" id="thisSelect">
									<option value="all">全員</option>
									##PublicLevel
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<button type="button" id="shareTimeConfirm" class="btn btn-primary">シェアする</button>
						<button type="button" id="shareTimeCancel" class="btn btn-default">キャンセル</button>
					</div>
				</div>
			</div>
		*/}).toString().match(/\/\*([^]*)\*\//)[1];


		content = content.replace("##PublicLevel", publicLevel);
		content = content.replace("##Today", "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 h3 text-center'>"+today.format("MM/DD dddd")+"</div>");

		dialog.append(content);

		dialog.on("hidden.bs.modal", function(){
			dialog.remove();
		})

		dialog.modal();


		if(isBreakPoint("xs") || isBreakPoint("sm")){
			$(".modal-dialog").css("padding-top", "0");
		}

		$("#thisSelect").selecter({
			mobile:true
		});

		var state;
		var stateNode;
		var stateVal;

		$("#thisSelect").on("change", function(event){
			if($(this).val() && $(this).val().indexOf("all") >= 0){
				stateVal = $(this).val();
				stateNode = $(".selecter-item.selected");
				$(".selecter-item").addClass("selected");
				state = true;
			}else if(state){
				state = false;
				stateVal.splice(stateVal.indexOf("all"), 1);
				$(".selecter-item").removeClass("selected");
				stateNode.addClass("selected");
				$(this).val(stateVal);
				$($(".selecter-item")[$(".selecter-item").data("value").indexOf("all")]).removeClass("selected");
			}
		});

		var startTimeShare = "06:00";
		var endTimeShare = "18:00";

		$("#shareTimeConfirm").on("touchend click",function(){
			var todayIs = today.format("YYYY/MM/DD");

			var newEvent = {
				'title':user["showName"],
				'start':moment(todayIs+startTimeShare, "YYYY/MM/DDHH:mm").format(),
				'end':moment(todayIs+endTimeShare, "YYYY/MM/DDHH:mm").format(),
				description:{
					'owner_name':user['user_name'],
					'owner_id':user['_id'],
					'eventTimeString':todayIs + " " + startTimeShare + "~" + endTimeShare,
					'dayString':todayIs
				}
			}

			var addToFriends = [];

			var type;

			if(!$("#thisSelect").val()){
				showError({title:"公開範囲を指定してください", message:"公開範囲が指定されていません"});
			}else{
				if($("#thisSelect").val() && $("#thisSelect").val().indexOf("all") >= 0){
					type = 1
				}else{
					var checkedFriends = $("#thisSelect").val();
					for(var i = 0, len = checkedFriends.length; len > i ; i++){
						addToFriends.push({"id":checkedFriends[i]});
					}
					type = 2
				}

				addLeisure(newEvent, addToFriends, type, today);
				$(".modal").modal("hide");
			}

			return false;
		});

		$("#shareTimeCancel").on("touchend click",function(){
			dialog.modal("hide");

			return false;
		})

		var slider = document.getElementById('timeSlider');

		noUiSlider.create(slider, {
			start: [360, 1080],
			connect: true,
			step:30,
			range: {
				'max': 1440,
				'min': 0
			}
		});

		slider.noUiSlider.on("update", function(values, handle){
			var startValue = values[0];
			var endValue = values[1];

			var hourStart = Math.floor(startValue / 60);
			var hourEnd = Math.floor(endValue / 60);

			var minStart = startValue - (hourStart * 60) == 0 ? "00":startValue - (hourStart * 60);
			var minEnd = endValue - (hourEnd * 60) == 0 ? "00":endValue - (hourEnd * 60);

			startTimeShare = startValue < 600 ? "0"+hourStart+ ":" + minStart : hourStart + ":" + minStart;
			endTimeShare = endValue < 600 ? "0"+hourEnd+":"+minStart : hourEnd+":"+minEnd;

			$("#startTimeShare").html(startTimeShare);
			$("#endTimeShare").html(endTimeShare);
		})

		dialog.modal();
	}

	var calendarRerender = function(type, display){
		var eventSources;

		switch (type){
			case "my":
				eventSources = [{
					events: display ? display:calendar.myEvents,
					color: "#008fbd"
				}];
				break;
			case "member":
				eventSources = [{
					events: display ? display:calendar.memberEvents,
					color: "#FFD700"
				}];
				break;
			case "group":
				eventSources = [{
					events: display ? display:calendar.groupEvents,
					color: "#44A728"
				}];
				break;
			default:
				eventSources = [
					{
						events: calendar.myEvents,
						color: "#008fbd"
					},
					{
						events: calendar.memberEvents,
						color: "#FFD700"
					},
					{
						events: calendar.groupEvents,
						color: "#44A728"
					}
				];
				break;
		}
		$("#calendar").fullCalendar("removeEvents", function(e){
			return true;
		});
		for(var i = 0, len = eventSources.length; len > i; i++){
			$("#calendar").fullCalendar('addEventSource', eventSources[i]);
		}
	}

	var addLeisure = function(addNewEvent, addToFriends, type, today){
		$.ajax({
			type:"POST",
			url:"/modeDBactionsloggedInIndex/addleisuretime",
			data:{
				"newEvent":addNewEvent,
				"addToFriends":JSON.stringify(addToFriends),
				"type":type
			},
			dataType:"json",
			success: function(json){
				calendar.myEvents.push(json);
				calendarRerender();
			}
		});
	}

	function prepareCalendarList(today, past, from, type){
		var myEvents = calendar["myEvents"];
		var memberEvents = calendar["memberEvents"];
		var groupEvents = calendar["groupEvents"];

		var timeTemplate;

		if(!past){
			timeTemplate = (function(){/*
			<a class="list-group-item clearfix">
				<span class="pull-right">
					<button class="btn btn-lg btn-primary ##Mytimes" data-index="##Index"><span class="glyphicon ##BtnIcon"></span></button>
				</span>
				<h4 class="list-group-item-heading">##Name</h4>
				<p class="list-group-item-text">##AboutTime</p>
				<div id="##ID" class="list-group" style="margin-top:10px;">
				</div>
			</a>
			*/}).toString().match(/\/\*([^]*)\*\//)[1];
			$("#MakeShareTime").removeClass("disabled");
		}else{
			timeTemplate = (function(){/*
			<a class="list-group-item disabled">
				<h4 class="list-group-item-heading">##Name</h4>
				<p class="list-group-item-text">終了しています</p>
				<div id="##ID" class="list-group">
				</div>
			</a>
			*/}).toString().match(/\/\*([^]*)\*\//)[1];
			$("#MakeShareTime").addClass("disabled");
		}

		var allDay = false;
		if(!today){
			allDay = true;
			today = moment();
			$("#mobileIndicatorDay").html("全て");
		}else{
			$("#mobileIndicatorDay").html(today.format("YYYY/MM/DD"));
			$("#mobileIndicatorDay").data("today", today.format("YYYY/MM/DD"));
		}

		var todayFormat = today.format("YYYY/MM/DD");

		function myEventsList(){
			var myEvents = calendar["myEvents"];
			$("#myPromiseMobile").empty();
			$("#mySharesMobile").empty();
			$("#offerCount").empty();

			var countPromise = 0;
			var countShare = 0;

			var timeMyTemplate = timeTemplate.replace("##Mytimes", "removePromise");
			timeMyTemplate = timeMyTemplate.replace("##BtnIcon", "glyphicon-trash");
			for(var i = 0, item,len = myEvents.length; len > i; i++){
				item = myEvents[i];
				if(item.description.dayString == todayFormat){
					if(item.description.promiseWith){
						countPromise++;
						if(item.description.owner_id == user["_id"]){
							var timeItem = timeMyTemplate.replace("##Name", item["description"]["owner_name"] + " With: " + item["description"]["promiseWith"]["name"]);
							timeItem = timeItem.replace("##AboutTime", item["description"]["eventTimeString"]);
							timeItem = timeItem.replace("##ID", item["_id"]);
							timeItem = timeItem.replace("##Index", i);
							$("#myPromiseMobile").append(timeItem);
						}else{
							var timeItem = timeMyTemplate.replace("##Name", item["description"]["owner_name"] + " With: " + item["description"]["promiseWith"]["name"]);
							if(!past){
								timeItem = timeItem.replace("##AboutTime", item["description"]["eventTimeString"]);
							}
							timeItem = timeItem.replace("##ID", item["_id"]);
							timeItem = timeItem.replace("##Index", i);
							$("#myPromiseMobile").append(timeItem);
						}
					}else{
						countShare++
						var timeItem = timeMyTemplate.replace("##Name", item["description"]["owner_name"]);
						timeItem = timeItem.replace("##AboutTime", item["description"]["eventTimeString"]);
						timeItem = timeItem.replace("##ID", item["_id"]);
						timeItem = timeItem.replace("##Index", i);
						$("#mySharesMobile").append(timeItem);
					}
				}
			}

			if(countPromise == 0){
				$("#myPromiseCount").html("");
				$("#myPromiseMobile").html("<a class='list-group-item disabled'>約束はありません</a>");
			}else{
				$("#myPromiseCount").html(countPromise)
			}

			if(countShare == 0){
				$("#mySharesCount").html("");
				$("#mySharesMobile").html("<a class='list-group-item disabled'>シェアしていません</a>");
			}else{
				$("#mySharesCount").html(countShare);
			}

			if((countShare > 0 || countPromise > 0) && !past){
				$(".removePromise").on("click touchend", function(){
					removeShareTimeMobile($(this).data("index"), myEventsList);

					return false;
				});
			}
		};

		function myFriendsEventList(){
			$("#friendsSharesMobile").empty();
			var memberEvents = calendar.memberEvents;
			var count = 0;
			var timeFriendsTemplate = timeTemplate.replace("##Mytimes", "sendOffer");
			timeFriendsTemplate = timeFriendsTemplate.replace("##BtnIcon", "glyphicon-share");
			timeFriendsTemplate = timeFriendsTemplate.replace("##ID", "");
			for(var i = 0, item, len = memberEvents.length; len > i; i++){
				item = memberEvents[i];
				if(item.description.dayString == todayFormat){
					var timeItem = timeFriendsTemplate.replace("##Name",item["title"]);
					timeItem = timeItem.replace("##AboutTime", item["description"]["eventTimeString"]);
					timeItem = timeItem.replace("##Index", i);
					$("#friendsSharesMobile").append(timeItem);
					count++;
				}
			}

			if(count == 0){
				$("#friendsSharesCount").html("");
				$("#friendsSharesMobile").html("<div class='list-group-item disabled'>友達のシェアはありません</div>")
			}else{
				$("#friendsSharesCount").html(count);
				if(!past){
					$(".sendOffer").on("click touchend", function(){
						sendOfferMsg($(this).data("index"));
						return false;
					});
				}
			}
		}

		function groupEventsList(){
			$("#groupEventsMobile").empty();
			var groupEvents = calendar.groupEvents;
			var count = 0;
			var timeGroupTemplate = timeTemplate.replace("##Mytimes", "detailEvent");
			timeGroupTemplate = timeGroupTemplate.replace("##ID", "");
			timeGroupTemplate = timeGroupTemplate.replace("##BtnIcon", "glyphicon-hand-up");
			for(var i = 0, item, len = groupEvents.length; len > i; i++){
				item = groupEvents[i];
				if(item.description.dayString == todayFormat){
					count++;
					var timeItem = timeGroupTemplate.replace("##Name", item["description"]["groupName"]);
					timeItem = timeItem.replace("##AboutTime", item["description"]["eventTimeString"]);
					timeItem = timeItem.replace("##Index", i);
					$("#groupEventsMobile").append(timeItem);
				}
			}

			if(count == 0){
				$("#groupEventsCount").html("")
				$("#groupEventsMobile").append("<div class='list-group-item disabled'>グループのイベントはありません</div>");
			}else{
				$("#groupEventsCount").html(count)
				if(!past){
					$(".detailEvent").on("click touchend", function(){
						var index = $(this).data("index");
						attendOrAbsenceChange(index);
						return false;
					});
				}
			}
		}

		switch (type){
			case "my":
				myEventsList();
				break;
			case "friends":
				myFriendsEventList();
				break;
			case "group":
				groupEventsList();
				break;
			default:
				myEventsList();
				myFriendsEventList();
				groupEventsList();
				break;
		}
	

		$.ajax({
			url:"/modeDBactionsloggedInIndex/getOfferMsgDay",
			type:"get",
			data:{
				day:today.format("YYYY/MM/DD"),
				allDay:allDay
			},
			dataType:"json",
			success:function(json){
				if(json.length > 0){
					var offerCount = 0;
					var template = (function(){/*
						<a class="list-group-item">
							<span class="pull-right">
								<button class="btn btn-lg btn-info offerMessages" data-day="##DayID" data-index="##Index"><span class="glyphicon glyphicon-check"></span></button>
							</span>
							<h4 class="list-group-item-heading">From: ##From</h4>
							<p class="list-group-item-text">##Context</p>
							<p class="list-group-item-text">##AboutTime</p>
						</a>
					*/}).toString().match(/\/\*([^]*)\*\//)[1];

					$.each(json, function(i, item){
						var offerMessage = template.replace(/##From/g, item["fromName"]);
						if(item['msg'].length > 10){
							offerMessage = offerMessage.replace("##Context", item["msg"].substr(0, 9) + "...");
						}else{
							offerMessage = offerMessage.replace("##Context", item["msg"]);
						}
						offerMessage = offerMessage.replace("##Index", i);
						offerMessage = offerMessage.replace("##AboutTime", item["aboutDate"]);
						offerMessage = offerMessage.replace("##DayID", item["day_id"]);
						if(document.getElementById(item["day_id"]) != null){
							offerCount++;
							$("#" + item["day_id"]).append(offerMessage);
						}
					});

					if(offerCount > 0){
						$("#offerCount").html(" :Offer <span class='badge'>"+ offerCount +"</span>");
					}

					$(".offerMessages").on("click touch",function(){
						var thisIndex = $("#" + $(this).data("day")).siblings("span").children("button").data("index");
						var thisModal = $('<div class="modal"></div>')
						var thisItem = json[$(this).data("index")];

						var offermodal = (function(){/*
							<div class="modal-dialog modal-xs" style="padding-top:100px;">
								<div class="modal-content col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="modal-header col-xs-12 col-sm-12">
										From: ##FromName
									</div>
									<div class="modal-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
										##Context
									</div>
									<div class="modal-footer col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<button type="button" id="offerConfirmBtn" class="btn btn-primary">約束する</button>
										<button type="button" class="btn btn-default" id="offerCancelBtn" data-dismiss="modal">キャンセル</button>
									</div>
								</div>
							</div>
						*/}).toString().match(/\/\*([^]*)\*\//)[1];

						offermodal = offermodal.replace("##FromName", thisItem["fromName"])
						offermodal = offermodal.replace("##Context", thisItem["msg"]);
						thisModal.append(offermodal);
						thisModal.modal()

						thisModal.on("hidden.bs.modal", function(){
							thisModal.remove();
						});

						$("#offerConfirmBtn").on("click touchend", function(){
							var fromID = thisItem["fromId"];
							var fromName = thisItem["fromName"];
							var dayId = thisItem["day_id"];
							$.ajax({
								url:"/modeDBactionsloggedInIndex/promiseOffer",
								type:"POST",
								data:{
									"fromID":fromID,
									"fromName":fromName,
									"myName":user["showName"],
									"dayID":dayId
								},
								dataType:"json",
								success:function(json){
									calendar.myEvents.splice(thisIndex, 1);
									calendar.myEvents.push(json);
									myEventsList();
									calendarRerender();
									$(".modal").modal("hide");
								}
							});

							return false;
						});


					});
				}


				if(from){
					from(today);
				}					
			}
		});

	}

	function mobileIndicatorShow(){
		$('html, body').scrollTop(0);
		$("#nextMonth").removeClass("visible-xs").addClass("hidden-xs");
		$("#MainContents").css("width", "200%");
		$("#Contents").removeClass("col-xs-12").css("width", "50%").css("float", "left");
		$("#mobileIndicator").addClass("visible-xs").css("width", "50%").css("float", "left");

		$("#Contents, #mobileIndicator").css({
			"transform": " translate(-"+ $(window).width() +"px)",
			"-moz-transform": " translate(-"+ $(window).width() +"px)",
			"-o-transform": " translate(-"+ $(window).width() +"px)",
			"-webkit-transform": " translate(-"+ $(window).width() +"px)",
			"-ms-transform": " translate(-"+ $(window).width() +"px)",
			"transition-duration":"600ms"
		});


		$("#Contents").one($.support.transition.end, function(){
			$("body").css({"overflow":"hidden"});
			$("#mobileIndicator").css("overflow", "scroll");
		});
	}


	function mobileIndicatorHide(){
		$('html, body').scrollTop(0);

		$("#Contents, #mobileIndicator").css({
			"transform": " translate(-"+ $(window).width() +"px)",
			"-moz-transform": " translate(-"+ $(window).width() +"px)",
			"-o-transform": " translate(-"+ $(window).width() +"px)",
			"-webkit-transform": " translate(-"+ $(window).width() +"px)",
			"-ms-transform": " translate(-"+ $(window).width() +"px)",
			"transition-duration":"0ms"
		});

		var f = $(window).width();

		$("#Contents").one($.support.transition.end, function(){
			$("body").css("overflow", "");
			$("#Contents, #mobileIndicator").removeAttr("style");
			$("#MainContents").css("width", "");
			$("#nextMonth").addClass("visible-xs").removeClass("hidden-xs");
			$("#mobileIndicator").removeClass("visible-xs");
			$(".collapse").collapse("hide");
			$("#Contents").removeClass("col-xs-6").addClass("col-xs-12");
		});

		$("#Contents, #mobileIndicator").css({
			"transform": "translate(0px)",
			"-moz-transform": "translate(0px)",
			"-o-transform": "translate(0px)",
			"-webkit-transform": "translate(0px)",
			"-ms-transform": "translate(0px)",
			"transition-duration":"600ms"
		});
	}

	function dialogShow(today){
		var dialog = $("<div class='modal' data-backdrop='static'></div>");
		var content = $((function(){/*
				<div class="modal-dialog" style="padding-top:100px;">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="thisDay"></h4>
						</div>
						<div class="modal-body" style="height:auto;max-height:475px;overflow-x:hidden;" id="thisModalContents">
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
						</div>
					</div>
				</div>
			*/}).toString().match(/\/\*([^]*)\*\//)[1]);
		dialog.append(content);
		dialog.modal();
		$("#thisModalContents").append($("#mobileDayDetail").clone(true));
		$("#thisModalContents .panel-collapse").addClass("in");
		dialog.on("hidden.bs.modal", function(){
			dialog.remove();
		});
	}


	function editGroupEvent(targetEvent, indexAt){
		var groupID = targetEvent["description"]["groupID"];
		var eventID = targetEvent["_id"];

		var startMoment = moment(targetEvent["start"]);
		var endMoment = moment(targetEvent["end"]);

		var startTime = (startMoment.hour() * 60) + startMoment.minute();
		var endTime = (endMoment.hour() * 60) + endMoment.minute();

		var dialog = $("<div class='modal'></div>");

		var date = new Date();
		var month = date.getMonth();
		var year = date.getFullYear();
		var daysTemplate;


		if(year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)){
			daysTemplate = [31,29,31,30,31,30,31,31,30,31,30,31];
		}else{
			daysTemplate = [31,28,31,30,31,30,31,31,30,31,30,31];
		}

		var eleMonth = "";
		var eleDays = {};

		for(var i = month; month + 3 > i; i++){
			var str;
			var dayStr = "";
			var dayCount = daysTemplate[i];

			if(i < 9){
				 str = "0" + (i+1);
			}else{
				str = i + 1;
			}


			eleMonth += "<li><input value='"+str+"' id='month"+str+"' name='monthInput' class='monthInputs' type='radio'><label for='month"+str+"'>" + str + "</label></li>"

			for(var j = 1 ; dayCount >= j ; j++){
				if(10 > j){
					dayStr += "<li><input value='0"+j+"' type='radio' class='dayInputs' name='dayInput' id='day"+j+"'><label for='day"+j+"'>0"+j+"</label></li>"
				}else{
					dayStr += "<li><input value='"+j+"' type='radio'  class='dayInputs' name='dayInput' id='day"+j+"'><label for='day"+j+"'>"+j+"</label></li>"
				}

				eleDays[str] = dayStr;
			}
		}

		var content = (function(){/*
			<div class="modal-dialog" style="padding-top:100px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">イベント作成</h4>
					</div>
					<div class="modal-body">
						<div class="text-center">
							<div class="input-group col-lg-12 col-md-12">
								<input id="eventTitle" placeholder="イベントタイトル：" class="form-control">
							</div>
							<div class="input-group col-lg-12 col-md-12" style="margin-top:15px;">
								<textarea id='eventContent' placeholder='イベント内容：' class="form-control" rows="5"></textarea>
							</div>
							<div class="row" style="margin-top:15px;margin-bottom:15px;">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
									<div class="btn-group col-lg-9 col-md-9 col-sm-9 col-xs-9">
										<button class="btn btn-default btn-lg btn-block dropdown-toggle" id="makeMonth" data-toggle="dropdown" id="monthsBtn">##ThisMonth<span class="caret"></span></button>
										<ul name="month" class="dropdown-menu" style="height:100px;" id="makeMonths">
										##EventMonth
										 </ul>
									</div>
									<h1 class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="margin-top:0;padding:0;">月</h1>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
									<div class="btn-group col-lg-9 col-md-9 col-sm-9 col-xs-9">
										<button class="btn btn-lg btn-default btn-block dropdown-toggle" data-toggle="dropdown" id="daysBtn">##ThisDay<span class="caret"></span></button>
										<ul name="day" class="dropdown-menu" id="makeDays" style="overflow-x:hidden;height:auto;max-height:200px;">
										##EventDay
										</ul>
									</div>
									<h1 class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="margin-top:0;padding:0;">日</h1>
								</div>
								<h1 id="weekday" class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="margin-top:0;padding:0;"></h1>
							</div>
							<div class="row" style="margin-top:10px;margin-bottom:10px;">
								<div id="timeSlider" class="noUiSlider col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-xs-10 col-sm-10 col-md-10 col-lg-10"></div>
								<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 text-right h3" id="eventStartTime">##StartEvent</div><div class="h3 col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">~</div><div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 h3 text-left" id="eventEndTime">##EndEvent</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="editGroupEventBtn" class="btn btn-warning">編集</button><button type="button" class="btn btn-danger" id="groupEventRemoveBtn">削除</btn><button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
					</div>
				</div>
			</div>
		*/}).toString().match(/\/\*([^]*)\*\//)[1];
		var dayParts = startMoment.format("MM-DD-dddd").split("-");
		content = content.replace("##StartEvent", startMoment.format("HH:mm"));
		content = content.replace("##EndEvent", endMoment.format("HH:mm"));
		content = content.replace("##ThisMonth", dayParts[0]);
		content = content.replace("##ThisDay", dayParts[1]);
		content = content.replace("##EventMonth", eleMonth);
		content = content.replace("##EventDay", eleDays[dayParts[0]]);

		dialog.append(content);
		dialog.modal();

		$(".monthInputs").val([dayParts[0]]);
		$(".dayInputs").val([dayParts[1]]);
		$("#weekday").html(dayParts[2]);

		$(".monthInputs").on("change", function(){
			$("#makeDays").empty().append(eleDays[$(this).val()]);
			$("#daysBtn").html("---<span class='caret'></span>");
			$("#weekday").empty();
		});

		$("#makeDays").on("change", function(){
			$("#weekday").html(moment($(".monthInputs:checked").val()+$(".dayInputs:checked").val(), "MMDD").format("dddd"));
		})

		$("#editGroupEventBtn").on("touchend click",function(){
			var eventDay = year+"/"+$(".monthInputs:checked").val()+"/"+$(".dayInputs:checked").val();
			var startEvent = moment(eventDay+startEventTime, "YYYY/MM/DDHH:mm");
			var endEvent = moment(eventDay+endEventTime, "YYYY/MM/DDHH:mm");


			if(Number(startEvent.format("YYYYMMDDHHmm")) < Number(moment().format("YYYYMMDDHHmm"))){
				$("#errMakeEvent").remove();
				dialog.append("<tr><td style='color:red' id='errMakeEvent'>過去の時間です</td></tr>")
			}else{
				var data = {
						newEventItem:{
							start:startEvent.format(),
							end:endEvent.format(),
							title:$("#eventTitle").val(),
							comment:$("#eventContent").val(),
							eventTimeString:eventDay + " " + startEventTime+"~"+endEventTime,
							dayString:eventDay,
						},
						editEventID:eventID,
						groupID:groupID
					}

				editGroupEventSend(data, indexAt);
			}

			return false;
		});

		$("#groupEventRemoveBtn").on("touchend click",function(){
			var data = {
				editEventID:eventID,
				groupID:groupID
			}


			var confirm = $("<div class='modal'></div>");
			var confirmContent = $((function(){/*
				<div class="alert alert-danger col-lg-offset-4 col-lg-4 col-md-4 col-md-offset-4" style="margin-top:200px;" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
  </button>					<p>イベントを削除しますか？</p>
					<p><button type="button" class="btn btn-danger" id="confirmRemoveEventBtn">削除</button><button type="button" class="btn btn-link" data-dismiss="alert">キャンセル</button></p>
				</div>
			*/}).toString().match(/\/\*([^]*)\*\//)[1]);

			confirmContent.alert();

			$("#confirmRemoveEventBtn").on("touchend click",function(){
				function modalHide(){
					calendar.groupEvents.splice(indexAt, 1);
					dialog.modal("hide");
				}
				var callBacks = [modalHide, callBack];

				editGroupEventSend(data, [modalHide, callBack]);
			});

			confirm.on("closed.bs.alert", function(){
				confirm.remove();
			});

			return false;
		});

		dialog.on("hidden.bs.modal", function(){
			dialog.remove();
		});

		$("#eventTitle").val(targetEvent["title"]);
		$("#eventContent").val(targetEvent["description"]["comment"]);

		var startEventTime = startMoment.format("HH:mm");
		var endEventTime = endMoment.format("HH:mm");


		var slider = document.getElementById('timeSlider');

		noUiSlider.create(slider, {
			start: [startTime, endTime],
			connect: true,
			step:30,
			range: {
				'max': 1440,
				'min': 0
			}
		});

		slider.noUiSlider.on("update", function(values, handle){
			var startValue = values[0];
			var endValue = values[1];

			var hourStart = Math.floor(startValue / 60);
			var hourEnd = Math.floor(endValue / 60);

			var minStart = startValue - (hourStart * 60) == 0 ? "00":startValue - (hourStart * 60);
			var minEnd = endValue - (hourEnd * 60) == 0 ? "00":endValue - (hourEnd * 60);

			startEventTime = String(hourStart).length == 1 ? "0"+hourStart+ ":" + minStart : hourStart + ":" + minStart;
			endEventTime = String(hourEnd).length == 1 ? "0"+hourEnd+":"+minStart : hourEnd+":"+minEnd;

			$("#eventStartTime").html(startEventTime);
			$("#eventEndTime").html(endEventTime);
		})

	}


	function editGroupEventSend(data, indexAt){
		$.ajax({
			url:"/modeDBactionsloggedInIndex/editGroupEvent",
			type:"post",
			data:data,
			dataType:"json",
			success:function(json){
				calendar.groupEvents.splice(indexAt, 1);
				calendar.groupEvents.push(json.edited);
				$(".modal").modal("hide");
				$("#calendar").fullCalendar("destroy");
				prepareCalendarList(moment(json.edited.start), null, null, "group");
				calendarRerender();
			}
		});
	}

	var attendOrAbsenceChange = function(indexAt){
		var targetEvent = calendar.groupEvents[indexAt];
		var dialog = $("<div style='width:100%;width:100%;' class='modal'></div>");

		var eventID = targetEvent["_id"];
		var groupID = targetEvent["description"]["groupID"];

		var content = (function(){/*
			<div class="modal-dialog" style="padding-top:100px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">イベント詳細</h4>
					</div>
					<div class="modal-body">
						<div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">イベント名：<h4>##EventTitle</h4></div>
						<div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">提案者： <h4>##EventSuggestionBy</h4></div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">日付： <h4>##EventDay</h4></div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">時間： <h4>##EventTime</h4></div>
						詳細：
						<div class="panel panel-default">
							<div class="panel-body" style="height:auto;max-height:150px;overflow-x:hidden;padding:10px;">
							##EventContent
							</div>
						</div>
						出席者：
						<ul class="list-group" style="height:auto;max-height:150px;overflow-x:hidden;">
							##EventAttendNow
						</ul>
					</div>
					<div class="modal-footer" id="thisFooter">
						##ButtonSet
					</div>
				</div>
			</div>
		*/}).toString().match(/\/\*([^]*)\*\//)[1];


		var buttonSet = '<button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>'

		dialog.on("hidden.bs.modal", function(){
			dialog.remove();
		});


		$.ajax({
			url:"/modeDBactionsloggedInIndex/getEventDetail",
			data:{id:eventID},
			dataType:"json",
			type:"get",
			success:function(events){
				var times = events["description"]["eventTimeString"].split(" ");
				content = content.replace("##EventTitle", events["title"]);
				content = content.replace(/##EventContent/g, events["description"]["comment"].replace(/\n/g, "<br>"));
				content = content.replace("##EventSuggestionBy", events["description"]["suggestionBy"]["showName"]);
				content = content.replace("##EventDay", times[0]);
				content = content.replace("##EventTime", times[1]);
				var mem = events.description.attend;
				var member = "";
				for(var i = 0; mem.length > i; i++){
					member += "<li class='list-group-item'>"+ mem[i]["showName"] + "</li>";
				}
				content = content.replace("##EventAttendNow", member);

				if(user._id === targetEvent.description.suggestionBy){
					buttonSet = "<button type='button' class='btn btn-warning' id='editGroupEvent'>編集</button>" + buttonSet;
					content = content.replace("##ButtonSet", buttonSet);

					dialog.append(content);
					dialog.modal();

					$("#editGroupEvent").on("click touchend", function(){
						editGroupEvent(targetEvent, indexAt);
						dialog.modal("hide");
						return false;
					});
				}else if(array_in_id(events.description.attend,user["_id"])){
					buttonSet = '<button type="button" class="btn btn-danger" id="absenceChange">欠席</button>' + buttonSet;
					content = content.replace("##ButtonSet", buttonSet);

					dialog.append(content);
					dialog.modal();

					$("#absenceChange").one("touchend click",function(){
						dialog.modal("hide");
						$.ajax({
							type:"POST",
							dataType:"json",
							data:{
								"eventID":eventID,
								"noticeId":"none"
							},
							url:"/modeDBactionsloggedInIndex/absenceGroupEvent",
							success:function(json){
							}
						});

						return false;
					});
				}else{
					buttonSet ='<button type="button" class="btn btn-primary" id="attendChange">出席</button>' + buttonSet;
					content = content.replace("##ButtonSet", buttonSet);

					dialog.append(content);
					dialog.modal();

					$("#attendChange").one("touchend click",function(){
						var data = {
							"eventID":eventID,
							"groupID":groupID,
							"name":user["showName"],
							"noticeId":"none"
						};
						dialog.modal("hide");
						$.ajax({
							type:"POST",
							data:data,
							dataType:"json",
							url:"/modeDBactionsloggedInIndex/attendGroupEvent",
							success:function(json){
							}
						});

						return false;
					});
				}

			}
		});
	}

	var array_in_id = function(arr, id){
		for(var i = 0 ; arr.length > i; i++){
			if(arr[i]["_id"] == id){
				return true;
			}
		}

		return false;
	}


	function sendOfferMsg(indexAt){
		var eventItem = calendar.memberEvents[indexAt];
		var eventDay = eventItem["description"]["dayString"];
		var eventTime = eventItem["description"]["eventTimeString"];
		var eventId = eventItem["_id"];
		var offerToName;
		var offerToId = eventItem["description"]["owner_id"];

		for(var i = 0, len = user.friends.length; len > i; i++){
			if(offerToId === user.friends[i]._id){
				offerToName = user.friends[i].showName;
				break;
			}
		}


		var dialog = $('<div class="modal"></div>');

		var content = (function(){/*
			<div class="modal-dialog" style="padding-top:100px;" id="modalWindow">
				<div class="modal-content">
					<div class="modal-header text-center">
						##OfferToName：##EventTime
					</div>
					<div class="modal-body text-center">
						<textarea name="offerMsg" id="offerMsgContent" style="resize:none;width:100%;" class="form-control" rows="10"></textarea>
					</div>
					<div class="modal-footer">
						<button type="button" id="sendOfferMsgBtn" class="btn btn-primary">オファーする</button><button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
					</div>
				</div>
			</div>
		*/}).toString().match(/\/\*([^]*)\*\//)[1];


		content = content.replace("##OfferToName", offerToName);
		content = content.replace("##EventTime", eventTime);

		dialog.append(content);

		dialog.on("hidden.bs.modal", function(){
			$("body").removeClass("modal-open");
			dialog.remove();
		});

		dialog.on("show.bs.modal", function(){
			$("body").addClass("modal-open");
		});

		dialog.modal();

		if(isBreakPoint('xs')){
			$("#modalWindow").css("padding-top", "10px");
		}

		$("#sendOfferMsgBtn").on("touchend click",function(){
			var msg = $("#offerMsgContent").val();
			var date = moment().format("YYYY-MM-DD HH:mm:ss");
			$.ajax({
				type:"POST",
				url:"/modeDBactionsloggedInIndex/sendOfferMsg",
				data:{
					"msg":msg,
					"toId":offerToId,
					"toName":offerToName,
					"fromName":user["user_name"],
					"dayId":eventId,
					"date":date,
					"aboutDate":eventTime,
					"aboutDay":eventDay
				},
				dataType:"json",
				success: function(json){
					dialog.modal("hide");
				}
			});

			return false;
		});
	}
	
	function allList(){
		noticeList();
		groupList();
		memberList();
	}


	function noticeList(type){
		var noticeCounter = 0;
		noticeCounter += user["noticeGroupEvent"].length;
		noticeCounter += user["noticeFriends"].length;
		noticeCounter += user["notice"].length;
		noticeCounter += user["noticeGroupInvite"].length

		if(noticeCounter > 0){
			$(".noticeBadge").html(noticeCounter);
		}else{
			$(".noticeBadge").html("");
		}

		var noticeTemplate = (function(){/*
			<a class="list-group-item ##Class clearfix" href="#" data-index="##Index">
				<h5 class="list-group-item-heading">##Message</h5>
				<p class="list-group-item-text">##Note</p>
			</a>
		*/}).toString().match(/\/\*([^]*)\*\//)[1];

		function groupEventNotice(){
			if(user["noticeGroupEvent"].length > 0){
				for(var i = 0, len = user["noticeGroupEvent"].length, item; len > i; i++){
					item = user["noticeGroupEvent"][i];
					if(item["type"] == "new"){
						var notices = noticeTemplate.replace("##Index", i);
						notices = notices.replace("##Class", "groupEventNotice");
						notices = notices.replace("##Message", item["groupName"] + "が新しくイベントを企画をしました");
						notices = notices.replace("##Note", item["note"]);
						$(".noticeGroupEvent").append(notices);
					}else if(item["type"] == "edit"){
						var notices = noticeTemplate.replace("##Index", i);
						notices = notices.replace("##Class", "editGroupEventNotice");
						notices = notices.replace("##Message", item["groupName"] + "がイベントを変更しました");
						notices = notices.replace("##Note", item["note"] + "の詳細を変更しました")
						$(".noticeEditEvent").append(notices);
					}
				}



				$(".groupEventNotice, .editGroupEventNotice").on("touchmove", function(){isTouch = false}).on("click touchend",function(){
					if(isTouch){
						attendOrAbsence($(this).data("index"));
					}else{
						isTouch = true;
					}

					return false;
				});
			}
		}

		function requestFriendNotice(){
			if(user["noticeFriends"].length > 0){
				for(var i = 0, len = user["noticeFriends"].length, item; len > i; i++){
					item = user["noticeFriends"][i];
					var notices = noticeTemplate.replace("##Class", "requestList");
					notices = notices.replace("##Index", i);
					notices = notices.replace("##Message", "友達申請があります");
					notices = notices.replace("##Note", "From: " + item["showName"]);
					$(".userAddNotice").append(notices);
				}

				$(".requestList").on("touchmove", function(){isTouch = false}).on("touchend click",function(){
					if(isTouch){
						showRequestListDialog($(this).data("index"));
					}else{
						isTouch = true;
					}

					return false;
				});
			}
		}

		function simpleNotice(){
			if(user["notice"].length > 0){
				for(var i = 0, len = user["notice"].length, item; len > i; i++){
					item = user.notice[i];
					if(item["type"] === "confirmPromise"){
						var notices = noticeTemplate.replace("##Class", "relationShares");
						notices = notices.replace("##Index", i);
						notices = notices.replace("##Message", item["name"] + "があなたからのオファーを約束しました");
						notices = notices.replace("##Note", "About: " + item["note"]);
						$(".userNotice").append(notices);
					}else if(item["type"] === "receiveNewOffer"){
						var notices = noticeTemplate.replace("##Class", "relationShares");
						notices = notices.replace("##Index", i);
						notices = notices.replace("##Message", item["name"] + "からオファーがあります");
						notices = notices.replace("##Note", "About: " + item["note"]);
						$(".userNotice").append(notices);
					}else if(item.type === "acceptRequest"){
						var notices = noticeTemplate.replace("##Class", "relationShares acceptRequest");
						notices = notices.replace("##Index", i);
						notices = notices.replace("##Message", item["name"] + "と友達になりました");
						notices = notices.replace("##Note", "　");
						$(".userNotice").append(notices);
					}else{
						var notices = noticeTemplate.replace("##Class", "relationShares");
						notices = notices.replace("##Index", i);
						notices = notices.replace("##Message", item["name"] + "が新しくシェアしました");
						notices = notices.replace("##Note", "About: " + item["note"]);
						$(".userNotice").append(notices);
					}
				}

				$(".relationShares").on("touchmove", function(){isTouch = false}).on("click touchend", function(){
					if(isTouch){
						if($(this).hasClass("acceptRequest")){
							var item = user.notice[$(this).data("index")];
							user.notice.splice($(this).data("index"), 1);
							showRequestDialog(item.userId);
							noticeList("notice")
						}else{
							var item = user.notice[$(this).data("index")],
							time = item.note.split(" ");
							var date = moment(time[0], "YYYY/MM/DD");
							if(isBreakPoint("xs") || isBreakPoint("sm")){
								mobileIndicatorShow();
								$("#navbarMenuItems").collapse("hide");
								prepareCalendarList(date, null, null);
							}else{
								prepareCalendarList(date, null, dialogShow);
							}
							user.notice.splice($(this).data("index"), 1);
							noticeList("notice");
						}
					}else{
						isTouch = true;
					}

					return false;
				});
			}
		}


		function groupInviteNotice(){
			if(user["noticeGroupInvite"].length > 0){
				for(var i = 0, len = user["noticeGroupInvite"].length, item; len >i ; i++){
					item = user["noticeGroupInvite"][i];
					var notices = noticeTemplate.replace("##Class", "noticeInviteGroup");
					notices = notices.replace("##Index", i);
					notices = notices.replace("##Message", item["groupName"] + "への招待があります");
					notices = notices.replace("##Note", "From: " + item["inviteFromName"]);
					$(".inviteGroup").append(notices);
				}

				$(".noticeInviteGroup").on("touchmove", function(){isTouch = false}).on("touchend click",function(){
					if(isTouch){
						showInviteGroup($(this).data("index"));
					}else{
						isTouch = true;
					}
				});
			}
		}

		switch (type){
			case "groupInvite":
				$(".inviteGroup").empty();
				groupInviteNotice();
				break;
			case "requestFriend":
				$(".userAddNotice").empty();
				requestFriendNotice();
				break;
			case "groupEvent":
				$(".noticeGroupEvent, .noticeEditEvent").empty();
				groupEventNotice();
				break;
			case "notice":
				$(".userNotice").empty();
				simpleNotice();
				break;
			default:
				groupInviteNotice();
				requestFriendNotice();
				groupEventNotice();
				simpleNotice();
				break;
		}
	}


	function groupList(){
		var groupListTemplate = (function(){/*
			<a class="list-group-item myGroupList clearfix" data-id="##ID">
				<span class="pull-right">
					<button class="btn btn-primary myGroupListDetail" data-index="##Index">詳細</button>
				</span>
				<h4 class="list-group-item-heading">##Name</h4>
			</a>
		*/}).toString().match(/\/\*([^]*)\*\//)[1];

		if(user["group"].length > 0){
			$(".groupListClass").empty();
		}


		for(var i = 0, len = user["group"].length, item = user["group"]; len > i; i++){
			var groupItem = groupListTemplate.replace("##ID", item[i]["_id"]);
			groupItem = groupItem.replace("##Name", item[i]["groupName"]);
			groupItem = groupItem.replace("##Leader", item[i].leader.showName);
			groupItem = groupItem.replace("##Index", i);
			$(".groupListClass").append(groupItem);
		}

		$(".myGroupList").on("touchmove", function(){isTouch = false}).on("click touchend", function(event){
			if(isTouch){
				var target = $(event.target);

				if(target.is("button")){
					var indexAt = target.data("index");
					clickGroupList(user["group"][indexAt]["_id"], user["group"][indexAt]["leader"]["_id"], user["group"][indexAt]["groupName"], indexAt);
				}else{
					clickShowGroupCalendar($(this).data("id"));
				}
			}else{
				isTouch = true;
			}

			return false;
		});
	}

	function memberList(){

		var friendsListTemplate = (function(){/*
			<a class="list-group-item myFriendsList clearfix" data-id="##ID">
				<span class="pull-right">
					<button class="btn btn-primary memberListDetail" >詳細</button>
				</span>
				<h4 class="list-group-item-heading">##Name</h4>
			</a>
		*/}).toString().match(/\/\*([^]*)\*\//)[1];

		if(user["friends"].length > 0){
			$(".memberListClass").empty();
		}

		$.each(user["friends"], function(i, item){
			var friendItem = friendsListTemplate.replace("##Name", item["showName"]);
			friendItem = friendItem.replace("##ID", item["_id"]);
			friendItem = friendItem.replace("##Index", i);
			$(".memberListClass").append(friendItem);
		});


		$(".myFriendsList").on("touchmove", function(){isTouch = false}).on("touchend click",function(event){
			if(isTouch){
				var target = $(event.target);

				if(target.is("button")){
					showRequestDialog($(this).data("id"));
				}else{
					clickMemberList($(this).data("id"));
				}
			}else{
				isTouch = true;
			}

			return false;
		});
	}

	var clickMemberList = function(id){
		var targetEvents = [];

		for(var i = 0;calendar["memberEvents"].length > i; i++){
			if(calendar["memberEvents"][i]["description"]["owner_id"] == id){
				targetEvents.push(calendar["memberEvents"][i]);
			}
		}

		if($("#mobileIndicator").is(":visible")){
			mobileIndicatorHide();
		}

		if($("#calendar").is(":hidden")){
			$("#calendar").show();
			$("#editProfile").hide();
			$("#myProfile").hide();
		}

		calendarRerender("member", targetEvents);
	}

	function clickShowGroupCalendar(id){
		var targetEvents = [];
		var groupCal = calendar["groupEvents"]

		for(var i = 0, len = groupCal.length; len > i; i++){
			if(groupCal[i]["description"]["groupID"] == id){
				targetEvents.push(groupCal[i]);
			}
		}

		if($("#mobileIndicator").is(":visible")){
			mobileIndicatorHide();
		}

		if($("#calendar").is(":hidden")){
			$("#calendar").show();
			$("#editProfile").hide();
			$("#myProfile").hide();
		}		

		calendarRerender("group", targetEvents);
	}

	function clickGroupList(id ,leader_id, groupName, index){
		var dialog = $("<div style='width:100%;' class='modal'></div>");
		var members;
		var groupName;

		var content = (function(){/*
			<div class="modal-dialog modal-md" style="padding-top:100px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body" style="height:auto;max-height:450px;">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<div>グループ名：<span id="thisGroupName" class="h4"></span></div>
								<div>リーダー名： <span id="thisGroupLeader" class="h4"></span></div>
								コメント：
								<div class="panel panel-default">
									<div id="thisGroupComment" style="max-height:150px;height:150px;overflow-x:hidden;" class="panel-body"></div>
								</div>
								メンバー：
								<ul id="thisGroupMember" style="max-height:200px;height:auto;overflow-x:hidden;" class="list-group"></ul>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						##anyButtons
					</div>
				</div>
			</div>
		*/}).toString().match(/\/\*([^]*)\*\//)[1];

		var buttonSet = "<button class='btn btn-primary' id='inviteFriendsToGroup'>友達を招待する</button><button class='btn btn-primary' id='makeNewEventBtn'>イベント作成</button><button class='btn btn-default' aria-hidden='true' data-dismiss='modal'>閉じる</button>"

		if(leader_id == user["_id"]){
			buttonSet = "<button class='btn btn-warning' id='editThisGroup' type='button'>グループ編集</button>" + buttonSet;
		}

		content = content.replace("##anyButtons", buttonSet);
		dialog.append(content);
		dialog.on("hidden.bs.modal", function(event){
			dialog.remove();
		});
		dialog.modal();

		if(isBreakPoint("xs") || isBreakPoint("sm")){
			$(".modal-dialog").css("padding-top", "0");
		}

		$("#editThisGroup").on("touchend click", function(){
			index = editGroups({"groupName":groupName, "groupComment":$("#thisGroupComment").html(), "groupID":id}, index) || index;
			return false;
		})

		$("#inviteFriendsToGroup").on("touchend click",function(){
			inviteFriends(id, groupName, members);
			return false;
		});

		$("#makeNewEventBtn").on("touchend click",function(){
			makeGroupEvent(id, groupName);
			return false;
		});

		$.ajax({
			type:"GET",
			url:"/modeDBactionsloggedInIndex/getGroupInfo",
			data:{
				"id":id
			},
			dataType:"json",
			success:function(group){
				members = group["member"];
				groupName = group["groupName"];
				$("#thisGroupName").html(group["groupName"]);
				$("#thisGroupLeader").html(group.leader.showName);
				for(var i = 0, len = group["member"].length; len > i; i++){
					$("#thisGroupMember").append($("<li class='list-group-item'>"+group["member"][i]["showName"]+"</li>"));
				}
				$("#thisGroupComment").html(group["comment"].replace(/\n/g, "<br>"));
			}
		});

	}

	function editGroups(groupItems, index){
		var dialog = $("<div class='modal'></div>");

		var content = (function(){/*
			<div class="modal-dialog" style="padding-top:100px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">グループ作成</h4>
					</div>
					<div class="modal-body">
						<div class="row" style="margin:10px auto;">
							<label for="groupName">グループ名</label>
							<input id="groupName" placeholder="グループ名：" type="text" class="form-control">
						</div>
						<div class="row" style="margin:10px auto;">
							<label for="groupComment">グループ紹介文</label>
							<textarea id="groupComment" placeholder="グループ紹介文：" rows="5" class="form-control"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="editGroupComfirmBtn">編集</button><button class="btn btn-default" data-dismiss="modal">キャンセル</button>
					</div>
				</div>
			</div>
		*/}).toString().match(/\/\*([^]*)\*\//)[1];

		dialog.append(content);

		dialog.one("hidden.bs.modal", function(){
			dialog.remove();
		})

		dialog.modal();

		if(isBreakPoint("xs") || isBreakPoint("sm")){
			$(".modal-dialog").css("padding-top", "0");
		}		

		$("#groupComment").val(groupItems["groupComment"].replace(/<br>/g, "\n"));
		$("#groupName").val(groupItems["groupName"]);

		$("#editGroupComfirmBtn").on("click touchend", function(){
			$.ajax({
				url:"/modeDBactionsloggedInIndex/editGroups",
				type:"POST",
				data:{
					newGroupName:$("#groupName").val(),
					newGroupComment:$("#groupComment").val(),
					groupID:groupItems["groupID"]
				},
				dataType:"json",
				success:function(newInfo){
					dialog.modal("hide");
					user["group"][index].groupName = newInfo.groupName;
					groupList();
					$("#thisGroupName").html(newInfo.groupName);
					$("#thisGroupLeader").html(newInfo.leader.showName);
					$("#thisGroupComment").html(newInfo.comment.replace(/\n/g, "<br>"));
					return user.group.length - 1;
				}
			});

			return false;
		});
	}

	function makeGroupEvent(id, groupName){
		var dialog = $("<div class='modal'></div>");

		var date = new Date();
		var month = date.getMonth();
		var year = date.getFullYear();
		var daysTemplate;


		if(year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)){
			daysTemplate = [31,29,31,30,31,30,31,31,30,31,30,31];
		}else{
			daysTemplate = [31,28,31,30,31,30,31,31,30,31,30,31];
		}

		var eleMonth = "";
		var eleDays = {};

		for(var i = month; month + 3 > i; i++){
			var str;
			var dayStr = "";
			var dayCount = daysTemplate[i];

			if(i < 9){
				 str = "0" + (i+1);
			}else{
				str = i + 1;
			}
			eleMonth += "<li><input value='"+ str +"' id='month"+str+"' name='monthInput' class='monthInputs' type='radio'><label for='month"+str+"'>" + str + "</label></li>"

			for(var j = ((i === month)? date.getDate() : 1); dayCount >= j ; j++){
				if(10 > j){
					dayStr += "<li><input value='0"+j+"' type='radio' class='dayInputs' name='dayInput' id='day"+j+"'><label for='day"+j+"'>0"+j+"</label></li>"
				}else{
					dayStr += "<li><input value='"+j+"' type='radio'  class='dayInputs' name='dayInput' id='day"+j+"'><label for='day"+j+"'>"+j+"</label></li>"
				}

				eleDays[str] = dayStr;
			}
		}

		var content = (function(){/*
			<div class="modal-dialog" style="padding-top:100px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">イベント作成</h4>
					</div>
					<div class="modal-body">
						<div class="text-center">
							<input id="eventTitle" placeholder="イベントタイトル：" class="form-control">
							<textarea id='eventContent' placeholder='イベント内容：' class="form-control" style="margin-top:15px;" rows="5"></textarea>
							<div class="row" style="margin-top:15px;margin-bottom:15px;">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
									<div class="btn-group col-lg-9 col-md-9 col-sm-9 col-xs-9">
										<button class="btn btn-default btn-lg btn-block dropdown-toggle" id="makeMonth" data-toggle="dropdown" id="monthsBtn">---<span class="caret"></span></button>
										<ul name="month" class="dropdown-menu" style="height:100px;">
											##EventMonth
										</ul>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 h1" style="margin-top:0;padding:0;">月</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
									<div class="btn-group col-lg-9 col-md-9 col-sm-9 col-xs-9">
										<button class="btn btn-lg btn-default btn-block dropdown-toggle" data-toggle="dropdown" id="daysBtn">---<span class="caret"></span></button>
										<ul name="day" class="dropdown-menu" id="makeDays" style="overflow-x:hidden;height:auto;max-height:200px;">

										</ul>
									</div>
									<div class="h1 col-lg-3 col-md-3 col-sm-3 col-xs-3" style="margin-top:0;padding:0;">日</div>
								</div>
								<div id="weekday" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center h1" style="margin-top:0;padding:0;"></div>
							</div>
							<div class="row" style="margin-top:10px;margin-bottom:10px;">
								<div id="timeSlider" class="noUiSlider col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-xs-10 col-sm-10 col-md-10 col-lg-10"></div>
								<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 text-right h3" id="eventStartTime"></div><div class="h3 col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">~</div><div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 h3 text-left" id="eventEndTime"></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="makeGroupEventBtn" class="btn btn-primary">イベント作成</button><button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
					</div>
				</div>
			</div>
		*/}).toString().match(/\/\*([^]*)\*\//)[1];

		content = content.replace("##EventMonth", eleMonth);

		dialog.append(content);

		dialog.on("hidden.bs.modal", function(){
			dialog.remove();
		});

		dialog.modal();

		if(isBreakPoint("xs") || isBreakPoint("sm")){
			$(".modal-dialog").css("padding-top", "");
		}

		var slider = document.getElementById("timeSlider");

		var startTimeEvent = "06:00";
		var endTimeEvent = "18:00";

		noUiSlider.create(slider, {
			start: [360, 1080],
			connect: true,
			step:30,
			range: {
				"max":1440,
				"min":0
			}
		});

		$(".monthInputs").on("change", function(){
			$("#makeDays").empty().append(eleDays[$(this).val()])
			$("#daysBtn").html("---<span class='caret'></span>")
			$("#weekday").empty();
		});

		$("#makeDays").on("change", "input", function(){
			$("#weekday").html(moment($(".monthInputs:checked").val()+$(this).val(), "MMDD").format("dddd"));
		})

		slider.noUiSlider.on("update", function(values, handle){
			var startValue = values[0];
			var endValue = values[1];

			var hourStart = Math.floor(startValue / 60);
			var hourEnd = Math.floor(endValue / 60);

			var minStart = startValue - (hourStart * 60) == 0 ? "00": "30";
			var minEnd = endValue - (hourEnd * 60)  == 0 ? "00" : "30";

			startTimeEvent = startValue < 600 ? "0" + hourStart + ":" + minStart : hourStart + ":" + minStart;
			endTimeEvent = endValue < 600 ? "0" + hourEnd + ":" + minEnd : hourEnd + ":" + minEnd;

			$("#eventStartTime").html(startTimeEvent);
			$("#eventEndTime").html(endTimeEvent);
		});


		$("#makeGroupEventBtn").on("touchend click",function(){
			var startTime = ""+moment().format("YYYY")+$(".monthInputs:checked").val()+$(".dayInputs:checked").val()+moment(startTimeEvent, "HH:mm").format("HHmm");
			var endTime = ""+moment().format("YYYY")+$(".monthInputs:checked").val()+$(".dayInputs:checked").val()+moment(endTimeEvent, "HH:mm").format("HHmm");

			if(!$(".monthInputs:checked").val() || !$(".dayInputs:checked").val()){
				showError({title:"日付が正しく選択されていません", message:"日付を選択してください"});
			}else if(!$("#eventTitle").val()){
				showError({title:"イベントのタイトルが入力されていません", message:"イベントのタイトルを入力してください"});
			}else if(!$("#eventContent").val()){
				showError({title:"イベント内容が入力されていません", message:"イベントの内容を入力してください"});
			}else{
				var data = {
					start:moment(startTime, "YYYYMMDDHHmm").format(),
					end:moment(endTime, "YYYYMMDDHHmm").format(),
					title:$("#eventTitle").val(),
					description:{
						comment:$("#eventContent").val(),
						eventTimeString:moment(startTime, "YYYYMMDDHHmm").format("YYYY/MM/DD HH:mm") + "~" + moment(endTime, "YYYYMMDDHHmm").format("HH:mm"),
						dayString:moment(startTime, "YYYYMMDD").format("YYYY/MM/DD"),
						groupID:id,
						groupName:groupName
					}
				}

				addGroupEvent(data);

				dialog.modal("hide");
			}

			return false;
		});

	}

	function checkNotice(){
		if($(".Notices").length == 0){
			$("#noticeArea").toggle();
		}
	}

	function displayError(message){
		if(!message){
			message = "エラーが発生しました。再読み込みをしてくださ。"
		}else{

		}
	}

	function addGroupEvent(data){
		$.ajax({
			type:"POST",
			url:"/modeDBactionsloggedInIndex/addGroupEvent",
			"data":data,
			dataType:"json",
			success:function(json){
				calendar["groupEvents"].push(json);
				calendarRerender();
			}
		});
	}

	$(document).ajaxError(function(){
		showError({title:"通信エラー", message:"ページを再読み込み、リロードをしてください"});
	});

	function showError(mes){
		$("#errorTitle").html(mes.title ? mes.title : "AnyError");
		$("#errorMessage").html(mes.message ? mes.message : "Error occur");
		var height = $(".navbar").height();
		$("#errorPanel").css({
			"-moz-transform": " translateY(-"+height+"px)",
			"-o-transform": " translateY(-"+height+"px)",
			"-webkit-transform": " translateY(-"+height+"px)",
			"-ms-transform": " translateY(-"+height+"px)",
			"transition-duration":"600ms"
		});

		$("#errorPanel").one($.support.transition.end, function(){
			setTimeout(function(){
				$("#errorPanel").css({
					"-moz-transform": " translateY(-200px)",
					"-o-transform": " translateY(-200px)",
					"-webkit-transform": " translateY(-200px)",
					"-ms-transform": " translateY(-200px)",
					"transition-duration":"800ms"
					});				
			}, 3000);
		});
	}
}());