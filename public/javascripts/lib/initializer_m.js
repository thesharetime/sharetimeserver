(function(){
	$(".thumbnails").removeAttr("style");
	$("#Contents").css({
		"padding":"0"
	});

	moment.locale('ja', {
		weekdays:["日曜日","月曜日","火曜日","水曜日","木曜日","金曜日","土曜日"],
		weekdaysShort:["日","月","火","水","木","金","土"]
	});

	$("body").css({
		"padding-top":$("#navbarHeader").height()
	});

	$("#nowMonthIs").html(moment().format("MMMM"));
	$("#nextMonthIs").html(moment().add(1, "M").format("MMMM"));
	$("#prevMonthIs").html(moment().add(-1, "M").format("MMMM"));
	$("#prevMonth").css("left", -deviceWidth/3).show();
	$("#nextMonth").css("right", -deviceWidth/3).show();

	$(".myProfileUserName").html(user["user_name"]);
	if(Number(user["accessTime"]) > 1){

		if(user["familyName"].length > 0){
			$(".familyName").val(user["familyName"]).html(user["familyName"]);
		}else{
			$("#myProfileFamilyName").html("未設定");
		}

		if(user["firstName"].length > 0){
			$(".firstName").val(user["firstName"]).html(user["firstName"]);
		}else{
			$("#myProfileFirstName").html("未設定");
		}

		if(user["familyName_hurigana"].length > 0){
			$(".familyName_hurigana").val(user["familyName_hurigana"]);
		}

		if(user["firstName_hurigana"].length > 0){
			$(".firstName_hurigana").val(user["firstName_hurigana"]);
		}

		if(user["showName"] != user["user_name"]){
			$(".showName").val(user["showName"]).html(user["showName"]);
		}else{
			$("#myProfileNickName").html("未設定");
		}

		if(user["comment"].length > 0){
			var comment = user["comment"].replace(/\n/g, "<br>");
			$("#myComment").html(comment);
			$("#profileComment").val(user["comment"]);
		}else{
			$("#myProfileComment").html("未設定");
		}

		if(user["birthDay"].length > 0){
			var birthDays = user["birthDay"].split("-");
			$("#myProfileBirthday").html(birthDays[0] + "年" + birthDays[1] + "月" + birthDays[2]);
			$(".profileYears").val([birthDays[0]]);
			$("#yearSelect").html(birthDays[0] + "<span class='caret'></span>");
			$(".profileMonths").val([birthDays[1]]);
			$("#monthSelect").html(birthDays[1] + "<span class='caret'></span>");
			$(".profileDays").val([birthDays[2]]);
			$("#daySelect").html(birthDays[2] + "<span class='caret'></span>");
		}
	}else{
		$("#calendar").hide()
		$("#editProfile").show()
		$("#editCancel").html("後で").one("click touchend",function(){
			$(this).text("キャンセル");
		});
	}

	$("#calendar").fullCalendar({
		header: {
			left: 'prev',
			center: 'title',
			right: 'next'
		},
		eventSources: [
		{
			events: calendar.myEvents,
			color: "#008fbd"
		},
		{
			events: calendar.memberEvents,
			color: "#FFD700"
		},
		{
			events: calendar.groupEvents,
			color: "#44A728"
		}
		],
		views:{
			month:{
				eventLimit:3
			}
		},
		aspectRatio:$("#calendar").width(),
		height:calHeight,
		dayClick: function(date, jsEvent, view){
			if(tmpEvent){
				return false;
			}else{
				if(isBreakPoint('xs') || isBreakPoint('sm')){
					if(isBreakPoint('xs')){
						mobileIndicatorShow();
					}

					showMobileIndicatorComponents("day");

					if(!$(jsEvent.target).hasClass("fc-past")){
						prepareCalendarList(date);
					}else{
						prepareCalendarList(date, "past");
					}
				}else{
					if(!$(jsEvent.target).hasClass("fc-past")){
						dialogShow(date);
					}else{
						dialogShow(date, "past");
					}
				}
			}
		},
		eventClick:function(event ,jsEvent, view){
			var date = moment(event["description"]["dayString"], "YYYY/MM/DD")
			if(isBreakPoint('xs') || isBreakPoint('sm')){
				if(isBreakPoint('xs')){
					mobileIndicatorShow();
				}
				showMobileIndicatorComponents("day");
				prepareCalendarList(date)
			}else{
				dialogShow(date);
			}
			return false;
		}
	});

	$(".fc-toolbar").hide();
	
	var script = document.creatElement("script");
	script.src = "/javascripts/lib/index.js";
	document.body.appendChild(script);
}());