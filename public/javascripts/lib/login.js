function ready(){

	function isBreakPoint(alias){
		return $(".device-"+alias).is(":visible")
	}

	$(window).resize(function(){
		if(isBreakPoint("sm") || isBreakPoint("xs")){
			$("#loginBox").css("margin-bottom", "200px");
		}else{
			$("#loginBox").css("margin-bottom", "");
		}
	});

	$(".stateInput").addClass("hidden");

	$("#loginDropdown").on("click touchend", function(){
		if($(".visible-xs, .visible-sm").is(":visible")){
			$("#signupMobileBtn").trigger("click");
		}else{
			$("#log_in_key").val($("#sign_up_email").val());
			$("#navbarLogin").dropdown("toggle");
			return false;
		}
	})



	$("#signupMobileBtn").on("click touchend", function(){
		$("#signup_box").toggle();
		if($("#login_box").is(":visible")){
			$("#login_box").addClass("hidden-xs hidden-sm").removeClass("visible-xs visible-sm");
			$("#signup_box").addClass("visible-xs visible-sm");
			$(this).html("ログイン");
		}else{
			$("#login_box").removeClass("hidden-xs hidden-sm").addClass("visible-xs visible-sm");
			$("#signup_box").removeClass("visible-xs visible-sm");
			$(this).html("登録する");
		}

		return false;
	})


	$("#sign_up_email").blur(function(){
		$("#signUpEmailDefault").hide();
		var email = $(this).val();
		if(!email.match(/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i)){
			$("#sign_up_button").attr("disabled", true);
			$("#signUpEmail").removeClass("has-success").addClass('has-error');
			$('.signUpEmailSuccess').removeClass("show").addClass("hidden");
			$('.signUpEmailFail').removeClass("hidden").addClass("show");
			$("#emailInFaile").html("不正なメールアドレスです");
		}else{
			$(".signUpEmail:visible").removeClass("show").addClass("hidden");
			$("#signUpEmailLoading").toggle();
			$.ajax({
				type:"POST",
				url:"/modeDBactionsloggedINpage/checkLoginItems",
				data:{
					"email":email
				},
				dataType: 'json',
				success:function(json){
				$("#signUpEmailLoading").toggle();
					if (json['state']){
						$("#signUpEmail").removeClass("has-error").addClass("has-success");
						$('.signUpEmailFail').removeClass("show").addClass("hidden");
						$('.signUpEmailSuccess').removeClass("hidden").addClass("show");
						$("#emailInFaile").hide();
						$("#emailInCorrect").show();
						if($("#usedEmail").is(":visible")){
							$("#usedEmail").collapse("hide");
						}
						if($("#userNameInCorrect").is(':visible') && $("#emailInCorrect").is(':visible')){
							$("#signupBtnLgMd, #signupBtnXsSm")
						}
					}else{
						$("#usedEmail").collapse("show");
						$("#signUpEmail").removeClass("has-success").addClass('has-error');
						$('.signUpEmailSuccess').removeClass("show").addClass("hidden");
						$('.signUpEmailFail').removeClass("hidden").addClass("show");
						$("#emailInFaile").html("既に登録済みのメールアドレスです");
					}
				}
			});
		}
	});


	$("#sign_up_userName").blur(function(){
		$("#signUpUserNameDefault").hide();
		var userName = $(this).val();
		if(!userName.match(/^[a-z\d_]{5,20}$/i)){
			$("#sign_up_button").attr("disabled", true);
			$("#signUpUserName").removeClass("has-success").addClass('has-error');
			$('.signUpUserNameSuccess').removeClass("show").addClass("hidden");
			$('.signUpUserNameFail').removeClass("hidden").addClass("show");
			$("#userNameInFaile").html("不正なユーザー名です");
		}else{
			$(".signUpUserName:visible").removeClass("show").addClass("hidden");
			$("#signUpUserNameLoading").toggle();
			$.ajax({
				type:"POST",
				url:"/modeDBactionsloggedINpage/checkLoginItems",
				data:{
					"user_name":userName
				},
				dataType: 'json',
				success:function(json){
					$("#signUpUserNameLoading").toggle();
					if (json['state']){
						$("#signUpUserName").removeClass("has-error").addClass("has-success");
						$('.signUpUserNameFail').removeClass("show").addClass("hidden");
						$('.signUpUserNameSuccess').removeClass("hidden").addClass("show");
						$("#userNameInFaile").hide();
						$("#userNameInCorrect").show();
						if($("#userNameInCorrect").is(':visible') && $("#emailInCorrect").is(':visible')){
							$("#sign_up_button").removeAttr("disabled");
						}
					}else{
						$("#signUpUserName").removeClass("has-success").addClass('has-error');
						$('.signUpUserNameSuccess').removeClass("show").addClass("hidden");
						$('.signUpUserNameFail').removeClass("hidden").addClass("show");
						$("#sing_up_button").attr("disabled", true);
						$("#userNameInFaile").html("既に使用されているユーザー名です");
					}
				}
			});
		}
	});

	$("#sign_up_password").focus(function(){
		$(this).val("");
		$("#signUpPassword").removeClass("has-error").removeClass("has-success");
		$("#passwordInCorrect").html("同じパスワードを入力してください");
		$("#signUpPasswordDefault").show();
		$(".signUpPassWordSuccess").removeClass("show").addClass("hide");
		$(".signUpPassWordFail").removeClass("show").addClass("hide");
		if($("#signUpPassWordRe").is(":visible")){
			$("#signUpPassWordRe").collapse("hide");
		}
	});

	$("#sign_up_passwordRe").focus(function(){
		$(this).val("");
	});

	$("#signUpPassWordRe").on("hidden.bs.collapse", function(){
		$("#sign_up_passwordRe").val("");
		$("#signUpPassWordRe").removeClass("has-error").removeClass("has-success");
	});

	$("#sign_up_password").on("keyup",function(){
		$("#signUpPasswordDefault").hide();
		if($("#sign_up_password").val().match(/(?=.*[a-zA-Z0-9]).{6,}/)){
			$("#signUpPassword").addClass('has-success').removeClass('has-error');
			$(".signUpPassWordSuccess").removeClass('hidden').addClass('show');
			$(".signUpPassWordFail").removeClass('show').addClass("hidden");
			$("#signUpPassWordRe").collapse("show");
		}else{
			if($("#sign_up_password").val().length < 6){
				$("#passwordInFail").html("6文字以上の英数字")
				$(".signUpPassWordFail").removeClass("hidden").addClass("show");
				$(".signUpPassWordSuccess").removeClass("show").addClass('hidden');
				$("#signUpPassword").addClass("has-error").removeClass("has-success");
			}
			if($("#signUpPassWordRe").is(":visible")){
				$("#signUpPassWordRe").collapse("hide");
			}
		}
	});

	$("#sign_up_passwordRe").on("keyup", function(){
		if($("#sign_up_passwordRe").val() == $("#sign_up_password").val()){
			$("#passwordInCorrect").html("有効なパスワードです");
			$("#signUpPassWordRe").addClass("has-success").removeClass("has-error");
			$(".signUpPassWordReSuccess").removeClass("hidden").addClass("show");
			$(".signUpPassWordReFail").removeClass("show").addClass("hidden");
		}else{
			$(".signUpPassWordReFail").removeClass("hidden").addClass("show");
			$(".signUpPassWordReSuccess").removeClass("show").addClass('hidden');
			$("#signUpPassWordRe").addClass("has-error").removeClass("has-success");
		}
	});

	$("#signupBtnLgMd, #signupBtnXsSm").on("click touchend", function(){
		if(!($("#sign_up_email").val().length > 0 && $("#emailInCorrect").is(":visible"))){
			$("#sign_up_email").focus();
		}else if(!($("#sign_up_userName").val().length > 0 && $("#userNameInCorrect").is(":visible"))){
			$("#sign_up_userName").focus();
		}else if(!($("#passwordInCorrect").is(":visible") && $("#sign_up_passwordRe").val() == $("#sign_up_password").val())){
			$("#sign_up_password").focus();
		}else{
			$.ajax({
				type:"post",
			 	url:"/modeDBactionsloggedINpage/signup",
			 	data:$("#signUpItems").serialize(),
			 	dataType:"json",
			 	success:function(json){
			 		if(json["message"]){
			 			$(location).attr("href", "/");
			 		}else if(json["type"] == "email"){
			 			$("#sign_up_email").focus();
			 		}else if(json["type"] == "userName"){
			 			$("#sign_up_userName").focus();
			 		}else if(json["type"] == "password"){
			 			$("#sign_up_password").focus();
			 		}else{
			 			$("#errorMsg").html("内部的なエラー");
			 			$("#errorBox").append("<p>内部的なエラーが起こりました</p><p>少し時間をおいてからお試しください</p>").toggle();
			 		}
			 	}
			 });
		}
	});
	$("form").submit(function(){
		$("<input >").attr("type", "hidden").attr("value", $("#thisTimeToken").val()).attr("name", "_csrf").appendTo(this);
	});

	$.ajaxPrefilter(function(options, originalOptions, jqXHR){
	  var token;
	  if(!options.crossDomain){
	    token = $("#thisTimeToken").val();
	    if(token){
	      return jqXHR.setRequestHeader("X-XSRF-Token", token);
	    }
	  }
	});
}

function errors(){
	$("#errorBox").append("<p>ログインに失敗しました</p><p>短時間で同じアカウントへのログインに失敗するとアカウントがロックされます</p><p><a href='/password/send'>パスワードをお忘れの場合</a></p>");
	$("#errorMsg").html("ログイン失敗");
	$("#errorBox").toggle();
}


/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD (Register as an anonymous module)
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var pluses = /\+/g;

    function encode(s) {
        return config.raw ? s : encodeURIComponent(s);
    }

    function decode(s) {
        return config.raw ? s : decodeURIComponent(s);
    }

    function stringifyCookieValue(value) {
        return encode(config.json ? JSON.stringify(value) : String(value));
    }

    function parseCookieValue(s) {
        if (s.indexOf('"') === 0) {
            // This is a quoted cookie as according to RFC2068, unescape...
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }

        try {
            // Replace server-side written pluses with spaces.
            // If we can't decode the cookie, ignore it, it's unusable.
            // If we can't parse the cookie, ignore it, it's unusable.
            s = decodeURIComponent(s.replace(pluses, ' '));
            return config.json ? JSON.parse(s) : s;
        } catch(e) {}
    }

    function read(s, converter) {
        var value = config.raw ? s : parseCookieValue(s);
        return $.isFunction(converter) ? converter(value) : value;
    }

    var config = $.cookie = function (key, value, options) {

        // Write

        if (arguments.length > 1 && !$.isFunction(value)) {
            options = $.extend({}, config.defaults, options);

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
            }

            return (document.cookie = [
                encode(key), '=', stringifyCookieValue(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path    ? '; path=' + options.path : '',
                options.domain  ? '; domain=' + options.domain : '',
                options.secure  ? '; secure' : ''
            ].join(''));
        }

        // Read

        var result = key ? undefined : {},
            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling $.cookie().
            cookies = document.cookie ? document.cookie.split('; ') : [],
            i = 0,
            l = cookies.length;

        for (; i < l; i++) {
            var parts = cookies[i].split('='),
                name = decode(parts.shift()),
                cookie = parts.join('=');

            if (key === name) {
                // If second argument (value) is a function it's a converter...
                result = read(cookie, value);
                break;
            }

            // Prevent storing a cookie that we couldn't decode.
            if (!key && (cookie = read(cookie)) !== undefined) {
                result[name] = cookie;
            }
        }

        return result;
    };

    config.defaults = {};

    $.removeCookie = function (key, options) {
        // Must not alter options, thus extending a fresh object...
        $.cookie(key, '', $.extend({}, options, { expires: -1 }));
        return !$.cookie(key);
    };

}));

/*! iCheck v1.0.1 by Damir Sultanov, http://git.io/arlzeA, MIT Licensed */
(function(f){function A(a,b,d){var c=a[0],g=/er/.test(d)?_indeterminate:/bl/.test(d)?n:k,e=d==_update?{checked:c[k],disabled:c[n],indeterminate:"true"==a.attr(_indeterminate)||"false"==a.attr(_determinate)}:c[g];if(/^(ch|di|in)/.test(d)&&!e)x(a,g);else if(/^(un|en|de)/.test(d)&&e)q(a,g);else if(d==_update)for(var f in e)e[f]?x(a,f,!0):q(a,f,!0);else if(!b||"toggle"==d){if(!b)a[_callback]("ifClicked");e?c[_type]!==r&&q(a,g):x(a,g)}}function x(a,b,d){var c=a[0],g=a.parent(),e=b==k,u=b==_indeterminate,
v=b==n,s=u?_determinate:e?y:"enabled",F=l(a,s+t(c[_type])),B=l(a,b+t(c[_type]));if(!0!==c[b]){if(!d&&b==k&&c[_type]==r&&c.name){var w=a.closest("form"),p='input[name="'+c.name+'"]',p=w.length?w.find(p):f(p);p.each(function(){this!==c&&f(this).data(m)&&q(f(this),b)})}u?(c[b]=!0,c[k]&&q(a,k,"force")):(d||(c[b]=!0),e&&c[_indeterminate]&&q(a,_indeterminate,!1));D(a,e,b,d)}c[n]&&l(a,_cursor,!0)&&g.find("."+C).css(_cursor,"default");g[_add](B||l(a,b)||"");g.attr("role")&&!u&&g.attr("aria-"+(v?n:k),"true");
g[_remove](F||l(a,s)||"")}function q(a,b,d){var c=a[0],g=a.parent(),e=b==k,f=b==_indeterminate,m=b==n,s=f?_determinate:e?y:"enabled",q=l(a,s+t(c[_type])),r=l(a,b+t(c[_type]));if(!1!==c[b]){if(f||!d||"force"==d)c[b]=!1;D(a,e,s,d)}!c[n]&&l(a,_cursor,!0)&&g.find("."+C).css(_cursor,"pointer");g[_remove](r||l(a,b)||"");g.attr("role")&&!f&&g.attr("aria-"+(m?n:k),"false");g[_add](q||l(a,s)||"")}function E(a,b){if(a.data(m)){a.parent().html(a.attr("style",a.data(m).s||""));if(b)a[_callback](b);a.off(".i").unwrap();
f(_label+'[for="'+a[0].id+'"]').add(a.closest(_label)).off(".i")}}function l(a,b,f){if(a.data(m))return a.data(m).o[b+(f?"":"Class")]}function t(a){return a.charAt(0).toUpperCase()+a.slice(1)}function D(a,b,f,c){if(!c){if(b)a[_callback]("ifToggled");a[_callback]("ifChanged")[_callback]("if"+t(f))}}var m="iCheck",C=m+"-helper",r="radio",k="checked",y="un"+k,n="disabled";_determinate="determinate";_indeterminate="in"+_determinate;_update="update";_type="type";_click="click";_touch="touchbegin.i touchend.i";
_add="addClass";_remove="removeClass";_callback="trigger";_label="label";_cursor="cursor";_mobile=/ipad|iphone|ipod|android|blackberry|windows phone|opera mini|silk/i.test(navigator.userAgent);f.fn[m]=function(a,b){var d='input[type="checkbox"], input[type="'+r+'"]',c=f(),g=function(a){a.each(function(){var a=f(this);c=a.is(d)?c.add(a):c.add(a.find(d))})};if(/^(check|uncheck|toggle|indeterminate|determinate|disable|enable|update|destroy)$/i.test(a))return a=a.toLowerCase(),g(this),c.each(function(){var c=
f(this);"destroy"==a?E(c,"ifDestroyed"):A(c,!0,a);f.isFunction(b)&&b()});if("object"!=typeof a&&a)return this;var e=f.extend({checkedClass:k,disabledClass:n,indeterminateClass:_indeterminate,labelHover:!0},a),l=e.handle,v=e.hoverClass||"hover",s=e.focusClass||"focus",t=e.activeClass||"active",B=!!e.labelHover,w=e.labelHoverClass||"hover",p=(""+e.increaseArea).replace("%","")|0;if("checkbox"==l||l==r)d='input[type="'+l+'"]';-50>p&&(p=-50);g(this);return c.each(function(){var a=f(this);E(a);var c=this,
b=c.id,g=-p+"%",d=100+2*p+"%",d={position:"absolute",top:g,left:g,display:"block",width:d,height:d,margin:0,padding:0,background:"#fff",border:0,opacity:0},g=_mobile?{position:"absolute",visibility:"hidden"}:p?d:{position:"absolute",opacity:0},l="checkbox"==c[_type]?e.checkboxClass||"icheckbox":e.radioClass||"i"+r,z=f(_label+'[for="'+b+'"]').add(a.closest(_label)),u=!!e.aria,y=m+"-"+Math.random().toString(36).substr(2,6),h='<div class="'+l+'" '+(u?'role="'+c[_type]+'" ':"");u&&z.each(function(){h+=
'aria-labelledby="';this.id?h+=this.id:(this.id=y,h+=y);h+='"'});h=a.wrap(h+"/>")[_callback]("ifCreated").parent().append(e.insert);d=f('<ins class="'+C+'"/>').css(d).appendTo(h);a.data(m,{o:e,s:a.attr("style")}).css(g);e.inheritClass&&h[_add](c.className||"");e.inheritID&&b&&h.attr("id",m+"-"+b);"static"==h.css("position")&&h.css("position","relative");A(a,!0,_update);if(z.length)z.on(_click+".i mouseover.i mouseout.i "+_touch,function(b){var d=b[_type],e=f(this);if(!c[n]){if(d==_click){if(f(b.target).is("a"))return;
A(a,!1,!0)}else B&&(/ut|nd/.test(d)?(h[_remove](v),e[_remove](w)):(h[_add](v),e[_add](w)));if(_mobile)b.stopPropagation();else return!1}});a.on(_click+".i focus.i blur.i keyup.i keydown.i keypress.i",function(b){var d=b[_type];b=b.keyCode;if(d==_click)return!1;if("keydown"==d&&32==b)return c[_type]==r&&c[k]||(c[k]?q(a,k):x(a,k)),!1;if("keyup"==d&&c[_type]==r)!c[k]&&x(a,k);else if(/us|ur/.test(d))h["blur"==d?_remove:_add](s)});d.on(_click+" mousedown mouseup mouseover mouseout "+_touch,function(b){var d=
b[_type],e=/wn|up/.test(d)?t:v;if(!c[n]){if(d==_click)A(a,!1,!0);else{if(/wn|er|in/.test(d))h[_add](e);else h[_remove](e+" "+t);if(z.length&&B&&e==v)z[/ut|nd/.test(d)?_remove:_add](w)}if(_mobile)b.stopPropagation();else return!1}})})}})(window.jQuery||window.Zepto);