function passwordRe(){
	$(".stateInput").addClass("hidden");
	
	$("#refleshPassword").on("focus",function(){
		$("#refleshPassword").val("");
		$("#refleshPasswordRe").val("");
		$("#refleshPasswordReInput").collapse("hide");
		$("#refleshPasswordDefault").removeClass("hidden").addClass("show");
		$(".refleshPasswordSuccess").removeClass("show").addClass("hidden");
		$(".refleshPasswordFail").removeClass("show").addClass("hidden");
		$("#refleshPasswordInput").removeClass("has-error").removeClass("has-success");
		$("#refleshPasswordInputRe").removeClass("has-error").removeClass("has-success");
	});
	
	$("#refleshPassword").on("keyup",function(){
		if($("#refleshPassword").val().length > 6){
			$("#refleshPasswordDefault").addClass("hidden");
			$("#refleshPasswordInput").addClass("has-success").removeClass("has-error");
			$("#refleshPasswordReInput").collapse("show");
			$(".refleshPasswordSuccess").removeClass("hidden").addClass("show");
			$(".refleshPasswordFail").removeClass("show").addClass("hidden");
		}else{
			$("#refleshPasswordDefault").addClass("hidden");
			$(".refleshPasswordFail").removeClass("hidden").addClass("show");
			$(".refleshPasswordSuccess").removeClass("show").addClass('hidden');
			$("#refleshPasswordInput").addClass("has-error").removeClass("has-success");
		}
	});
	
	$("#refleshPasswordRe").on("keyup", function(){
		if($(this).val() == $("#refleshPassword").val()){
			$("#refleshPasswordReInput").addClass("has-success").removeClass("has-error");
			$(".refleshPasswordReSuccess").removeClass("hidden").addClass("show");
			$(".refleshPasswordReFail").removeClass("show").addClass("hidden");
			$("#passwordInCorrect").html("有効なパスワードです");
		}else{
			$("#refleshPasswordReInput").addClass("has-error").removeClass("has-success");
			$(".refleshPasswordReFail").removeClass("hidden").addClass("show");
			$(".refleshPasswordReSuccess").removeClass("show").addClass('hidden');
		}
	});
	
	$("form").submit(function(){
		var pass = $("#refleshPassword").val();
		var passRe = $("#refleshPasswordRe").val();
		if(pass != passRe || pass.length < 7){
			$("#refleshPassword").focus();
			return false;
		}else{
			return true;
		}
	});
}