(function(){
	var tutorialPanel = $((function(){/*
		<div id="tutorialPanel" class="alert alert-success text-center" style="position:fixed;	-moz-transform:translateY(-200px);-o-transform:translateY(-200px);-webkit-transform:translateY(-200px);-ms-transform:translateY(-200px);width:100%;z-index: 15000;">
			<strong id="tutorialTitle"></strong>
			<p id="tutorialMessage"></p>
		</div>
	*/}).toString().match(/\/\*([^]*)\*\//)[1]);

	$("#MainContents").prepend(tutorialPanel);

	if(Number(user.accessTime) === 1){ 
		showTutorial({title:"シェアタイムへようこそ！", message:"カレンダーから日付をお選びになると詳細が表示されます！<br>まずはプロフィールを設定することで友達から見つけやすくしましょう！<br>後から設定することも可能です！", time:6000});

	}

	if(user["showName"] === user["user_name"]){
		$("#navbarMenuEditProfile").one("click touchend", function(){
			showTutorial({title:"プロフィールを設定しましょう！", message:"プロフィールを設定することで友達から検索されやすくなります！"})
		});
		$("#editConfirm, #editCancel").one("touchend click", function(){
			showTutorial({title:"さあ友達と時間を共有しましょう！", message:"プロフィールの設定が完了しました<br>カレンダーに戻り友達と時間を共有しましょう！"});
		});
	}

	if(user.friends.length < 1){
		$(".search-query").one("focus", function(){
			showTutorial({title:"友達を検索しましょう!", message:"友達の名前、または友達のユーザー名の先頭に@をつけて入力して検索しましょう<br><strong>例：</strong>@username、なまえ、姓　名前"});
		});
	}

	if(calendar.myEvents.length < 1){
		$("#MakeShareTime").one("touchend click", function(){
			showTutorial({title:"あなたの時間をシェアしましょう！", message:"あなたの時間を友達とシェアしましょう！<br>公開範囲を指定すると、指定した人とだけ共有できます！", time:5000})
		});
	}

	if(user.group.length < 1){
		$(".navigateGroupMenu").one("touchend click", function(){
			showTutorial({title:"グループを作成して友達を招待しましょう！", message:"グループを作成してイベントを企画し、友達を招待しましょう！"});
		});
	}

	$(".myGroupListDetail").one("touchend click", function(){
		showTutorial({title:"グループのイベント作成や友達を招待しましょう！", message:"イベントを作成することでグループのメンバーに通知することが可能です！<br>また、まだ未加入の友人がいればグループへ招待することできます！", time:5000});
	});

	function showTutorial(mes){
		$("#tutorialTitle").html(mes.title ? mes.title : "AnyTutorial");
		$("#tutorialMessage").html(mes.message ? mes.message : "Tutorial");
		var height = $(".navbar").height();

		$("#tutorialPanel").one($.support.transition.end, function(){
			setTimeout(function(){
				$("#tutorialPanel").css({
					"-moz-transform": " translateY(-200px)",
					"-o-transform": " translateY(-200px)",
					"-webkit-transform": " translateY(-200px)",
					"-ms-transform": " translateY(-200px)",
					"transition-duration":"800ms"
					});				
			}, mes.time ? mes.time : 3000);
		});

		$("#tutorialPanel").css({
			"-moz-transform": " translateY(-"+height+"px)",
			"-o-transform": " translateY(-"+height+"px)",
			"-webkit-transform": " translateY(-"+height+"px)",
			"-ms-transform": " translateY(-"+height+"px)",
			"transition-duration":"600ms"
		});
	}
}());