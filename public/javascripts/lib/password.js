function passwords(){
	$("#sendEmailBtn").one("click touchend", function(){
		$(this).prop("disabled", true);
		$.ajax({
			url:"/modeDBactionsPassword/passwordSend",
			type:"post",
			data:{"email":$("#email").val()},
			success:function(json){
				$("#sendDone, #sendBox").toggle();
				setTimeout(function(){
					$(location).attr("href", "/login");
				},5000);
			}
		})
	});

	$.ajaxPrefilter(function(options, originalOptions, jqXHR){
	  var token;
	  if(!options.crossDomain){
	    token = $("#thisTimeToken").val();
	    if(token){
	      return jqXHR.setRequestHeader("X-XSRF-Token", token);
	    }
	  }
	});
}
