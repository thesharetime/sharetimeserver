var jsjws = require('jsjws');
var crypto = require("crypto");
var bcrypt = require("bcrypt")
var assert = require("assert");
var conf = require('config');
var header = { alg: 'RS256' };
var Promise = require('bluebird');
var model = require("./model/database.js");
var priv_key;
var pub_key;

exports.keysMake = function(){
	return new Promise(function(resolve, reject){
		model.keyGenOrKeyGet().then(function(){
			var key = jsjws.generatePrivateKey(2048, 65537);
			var priv_pem = key.toPrivatePem('utf8');
			var pub_pem = key.toPublicPem('utf8');
			var priv_gem = jsjws.createPrivateKey(priv_pem, 'utf8');
			var pub_gem = jsjws.createPublicKey(pub_pem, 'utf8');
			model.savePublicKey(priv_pem, pub_pem);
			resolve({
				"priv_pem":priv_pem,
				"pub_pem":pub_pem
			});
		}).catch(function(keys){
			resolve({
				"priv_pem":keys.priv,
				"pub_pem":keys.pub
			});
		});
	});
}

exports.setKeys = function(keys){
	priv_key = jsjws.createPrivateKey(keys["priv_pem"], 'utf8');
	pub_key = jsjws.createPublicKey(keys["pub_pem"], 'utf8');
}


var cipherid = function(id){
	var cipher = crypto.createCipher('aes192', conf.PASS);
	cipher.update(JSON.stringify(id), 'utf8', 'hex');
	var cipheredId = cipher.final('hex');
	return cipheredId;
}

var decipherid = function(targetId){
	var id = String(targetId);
	console.log(id);
	var decipher = crypto.createDecipher('aes192', conf.PASS);
	decipher.update(id, 'hex', 'utf8');
	console.log(decipher);
	var dec = decipher.final('utf8');	
	return dec;
}

exports.deCipherId = function(accessToken){
	var jws = new jsjws.JWS();
	jws.verifyJWSByKey(accessToken, pub_key, ['RS256'])
}

exports.cipherPass = function(pass){
	return new Promise(function(resolve, reject){
		bcrypt.genSalt(10, function(err, salt){
			bcrypt.hash(pass, salt, function(err, hash){
				console.log(hash);
				resolve({"pass":hash, "salt":salt});
			});
		});
	});
}

exports.comparePass = function(password, passCiphered){
	return new Promise(function(resolve, reject){
		bcrypt.compare(password, passCiphered, function(err, res){
			resolve(res);
		});
	});
}



exports.makeAccessToken = function(userId){
	return new Promise(function(resolve, reject){
		var payload = {user_id:userId};
		var sig = new jsjws.JWS().generateJWSByKey(header, payload, priv_key);
		resolve({"accessToken":sig, "STid":userId});
	});
}



exports.verifyAccess = function(accessToken){
	var jws = new jsjws.JWS();
	try{
		if(jws.verifyJWSByKey(accessToken, pub_key, ['RS256'])){;
			var payload = JSON.parse(jws.getUnparsedPayload());
			var data = payload.user_id;
			return data;
		}
	}catch (e){
		console.log(e);
		return null;
	}
}
