var indexECT = require('./routes/index');
var loginECT = require('./routes/login');
var passwordECT = require('./routes/password')
var modelIndex = require('./model/index.js');
var modelLogin = require('./model/login.js');
var modelApplicationApi = require("./model/application.js");
var modelPass = require("./model/password");
var jws = require('./jws')

var checkLogin = function(req, res, next){
	if(jws.verifyAccess(req.cookies.ACCTK)){
		next()
	}else{
		res.redirect("/login");
	}
}

function forceHttps(req, res, next){
  if (!process.env.PORT) {
    return next();
  };

  if (req.headers['x-forwarded-proto'] && req.headers['x-forwarded-proto'] === "http" && !req.xhr) {
    res.redirect('https://' + req.headers.host + req.url);
  }else {
    return next();
  }
};


var sessionChecker = function(req, res, next){
	if(req.session){
		console.log(req.session);
	}
	req.session.errors = {test:"test"};
	next();
}



var routeConfiger = function(app, server){
	app.all("/*", forceHttps);

	app.get('/', checkLogin);
	app.get("/login", function(req, res, next){
		if(jws.verifyAccess(req.cookies.ACCTK)){
			res.redirect("/")
		}else{
			next();
		}
	});
	app.get('/logout', function(req, res, next){
		res.clearCookie("ACCTK");
		res.clearCookie("STID");
		res.redirect("/login");
	});
	app.get("/password/*", function(req, res, next){
		if(jws.verifyAccess(req.cookies.ACCTK)){
			res.redirect("/");
		}else{
			next();
		}
	});
	app.use('/', indexECT);
	app.use('/login', loginECT);
	app.use('/password', passwordECT);
	app.use("/application", modelApplicationApi);
	app.use("/modeDBactionsPassword/", modelPass)
	app.use('/modeDBactionsloggedINpage', modelLogin);
	app.use('/modeDBactionsloggedInIndex/', modelIndex);
	app.get("/*", function(req, res, next){
		var errMessage = req.session.errors ? req.session.errors.message : "PAGE IS NOT FOUND";
		delete req.session.errors
		res.render('error', {message:errMessage});
	});
}

exports.routeConfiger = routeConfiger;
