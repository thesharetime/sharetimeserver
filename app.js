var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var http = require('http');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();
var compress = require("compression")();
var csurf = require('csurf');
var cluster = require('cluster');
var jws = require('./jws');
var model = require('./model/database')
var Promise = require("bluebird");
var session = require("express-session");
var MongoStore = require("connect-mongo")(session);
var firstFlags = true;

var ECT = require('ect'); //追加
var ectRenderer = ECT({ watch: true, root: __dirname + '/views', ext : '.ect' }); //追加


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ect');
app.engine('ect', ectRenderer.render);

app.set('port', process.env.PORT || 8080);

app.use(logger('dev'));
app.use(compress);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(cookieParser());
app.use(session({
	secret: 'secrets',
	store: new MongoStore({mongooseConnection:model.mongooseConnection}),
	saveUninitialized:false,
	resave:true,
	cookie:{
		path:"/",
		httpOnly:false,
		secure:false,
		maxAge: 1000 * 60 * 60 * 24 *365
	}
}));

app.use(csurf());
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(path.join(__dirname + '/public/images/favicon.png')));

if(cluster.isMaster){
  var numCPUs = require("os").cpus().length;
  var DBid;

  jws.keysMake().then(function(keyItem){
    for(var i = 0; i < numCPUs ; i++){
      cluster.fork();
    }

    Object.keys(cluster.workers).forEach(function(id){
      cluster.workers[id].on("message", function(msg){
        cluster.workers[id].send(keyItem);
      });
    });
  });


  cluster.on("exit", function(){
    cluster.fork();
  });

}else{

  if(firstFlags){
    process.on("message", function(keyItem){
      jws.setKeys(keyItem);
    });

    process.send("here");
    firstFlags = false;
  }

  var server = http.createServer(app);

  var routes = require('./routeContorol.js');

  routes.routeConfiger(app, server);

  module.exports = app;

  server.listen(app.get('port'),function(){
    console.log("working on " + app.get('port'));
  })
}
